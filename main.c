/*
 *
 * Copyright (c) 2016 QIQI Labs. All Rights Reserved.
 * Atlanta, GA Boston, MA
 * Copyright (c) 2016 QIQI Labs. All Rights Reserved. EQ & SI
 *
*/

#include <stdint.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <math.h>
#include <time.h>
#include "nordic_common.h"
#include "nrf.h"
#include "app_error.h"
#include "nrf_gpio.h"
#include "nrf_adc.h"
#include "nrf_delay.h"
#include "nrf51_bitfields.h"
#include "nrf_gpio.h"
#include "ble.h"
#include "ble_hci.h"
#include "ble_srv_common.h"
#include "ble_advdata.h"
#include "ble_conn_params.h"
#include "boards.h"
#include "ble_advertising.h"
#include "softdevice_handler.h"
#include "device_manager.h"
#include "app_timer_appsh.h"
#include "ble_error_log.h"
#include "app_gpiote.h"
#include "app_button.h"
#include "ble_debug_assert_handler.h"
#include "pstorage.h"
#include "ble_lbs.h"
#include "ble_gap.h"
#include "app_uart.h"
#include "app_bond.h"
//#include "SEGGER_RTT.h"

#define IS_SRVC_CHANGED_CHARACT_PRESENT 0                                           /**< Include or not the service_changed characteristic. if not enabled, the server's database cannot be changed for the lifetime of the device*/

#define WAKEUP_BUTTON_PIN               BSP_BUTTON_0                                /**< Button used to wake up the application. */

#define ADVERTISING_LED_PIN_NO          BSP_LED_0                                   /**< Is on when device is advertising. */
#define CONNECTED_LED_PIN_NO            BSP_LED_1                                   /**< Is on when device has connected. */

#define LEDBUTTON_LED_PIN_NO            BSP_LED_0
#define LEDBUTTON_BUTTON_PIN_NO         BSP_BUTTON_1

#define DEVICE_NAME                     "QIQI Shower"                             	/**< Name of device. Will be included in the advertising data. */

//#define APP_ADV_INTERVAL                64                                        /**< The advertising interval (in units of 0.625 ms. This value corresponds to 40 ms). */
//#define APP_ADV_INTERVAL                1600                                        /**< The advertising interval (in units of 0.625 ms. This value corresponds to 40 ms). */
#define APP_ADV_TIMEOUT_IN_SECONDS      180                                         /**< The advertising timeout (in units of seconds). */

#define QIQI_APP_ADV_FAST_INTERVAL		500/0.625									/* Units = ms/0.625 */
#define APP_ADV_FAST_INTERVAL           QIQI_APP_ADV_FAST_INTERVAL                  /**< Fast advertising interval (in units of 0.625 ms. This value corresponds to 25 ms.). */

#define QIQI_APP_ADV_SLOW_INTERVAL		2000/0.625
#define APP_ADV_SLOW_INTERVAL           QIQI_APP_ADV_SLOW_INTERVAL                  /**< Slow advertising interval (in units of 0.625 ms. This value corresponds to 2 seconds). */

#define APP_ADV_FAST_TIMEOUT            30                                          /**< The duration of the fast advertising period (in seconds). */
#define APP_ADV_SLOW_TIMEOUT            180                                         /**< The duration of the slow advertising period (in seconds). */

#define APP_ADV_DIRECTED_TIMEOUT        5                                           /**< Number of direct advertisement (each lasting 1.28seconds). */

#define APP_TIMER_PRESCALER             0											/**< Value of the RTC1 PRESCALER register. */
#define APP_TIMER_MAX_TIMERS            6                                           /**< Maximum number of simultaneously created timers. */
#define APP_TIMER_OP_QUEUE_SIZE         6                                           /**< Size of timer operation queues. */

#define MIN_CONN_INTERVAL               MSEC_TO_UNITS(500, UNIT_1_25_MS)            /**< Minimum acceptable connection interval (0.5 seconds). */
#define MAX_CONN_INTERVAL               MSEC_TO_UNITS(1000, UNIT_1_25_MS)            /**< Maximum acceptable connection interval (1 second). */
#define SLAVE_LATENCY                   0                                           /**< Slave latency. */
#define CONN_SUP_TIMEOUT                MSEC_TO_UNITS(4000, UNIT_10_MS)             /**< Connection supervisory timeout (4 seconds). */
#define FIRST_CONN_PARAMS_UPDATE_DELAY  APP_TIMER_TICKS(20000, APP_TIMER_PRESCALER) /**< Time from initiating event (connect or start of notification) to first time sd_ble_gap_conn_param_update is called (15 seconds). */
#define NEXT_CONN_PARAMS_UPDATE_DELAY   APP_TIMER_TICKS(5000, APP_TIMER_PRESCALER)  /**< Time between each call to sd_ble_gap_conn_param_update after the first call (5 seconds). */
#define MAX_CONN_PARAMS_UPDATE_COUNT    3                                           /**< Number of attempts before giving up the connection parameter negotiation. */

#define APP_GPIOTE_MAX_USERS            1                                           /**< Maximum number of users of the GPIOTE handler. */

#define BUTTON_DETECTION_DELAY          APP_TIMER_TICKS(50, APP_TIMER_PRESCALER)    /**< Delay from a GPIOTE event until a button is reported as pushed (in number of timer ticks). */

#define SEC_PARAM_TIMEOUT               30                                          /**< Timeout for Pairing Request or Security Request (in seconds). */
#define SEC_PARAM_BOND                  1                                           /**< Perform bonding. */
#define SEC_PARAM_MITM                  0                                           /**< Man In The Middle protection not required. */
#define SEC_PARAM_IO_CAPABILITIES       BLE_GAP_IO_CAPS_NONE                        /**< No I/O capabilities. */
//#define SEC_PARAM_IO_CAPABILITIES       BLE_GAP_IO_CAPS_DISPLAY_ONLY                        /**< No I/O capabilities. */

#define SEC_PARAM_OOB                   0                                           /**< Out Of Band data not available. */
#define SEC_PARAM_MIN_KEY_SIZE          7                                           /**< Minimum encryption key size. */
#define SEC_PARAM_MAX_KEY_SIZE          16                                          /**< Maximum encryption key size. */

#define DEAD_BEEF                       0xDEADBEEF                                  /**< Value used as error code on stack dump, can be used to identify stack location on stack unwind. */

#define MAX_TEST_DATA_BYTES     		(15U)                						/**< max number of test bytes to be used for tx and rx. */
#define UART_TX_BUF_SIZE 				512                         				/**< UART TX buffer size. */
#define UART_RX_BUF_SIZE 				512                           				/**< UART RX buffer size. */
//
//  Shower Settings
//
#define VARIANCE_TARGET 				0.35
#define SAMPLE_TARGET  					10
#define PRECHECK_TEMP  					90 	//Change to 90!!!!
#define SHOWERCUTOFFADJUST 				4
#define SCALDING_TEMPERATURE			104	//Change to 104!!!
#define SHOWERISHOT						91  //Change to 91!!!!
#define WUT_NOTIFY_PERIOD				3	// Every 3 readings
//
//	Battery Levels
//
#define BATTERY_LOW						7.0
#define BATTERY_CRITICAL				6.4
//
// State Machine Definitions
//
#define STATE_INIT    					0
#define STATE_READY   					1
#define STATE_WARMUP  					2
#define STATE_WAIT    					3
#define STATE_BATTLOW 					5
#define STATE_ON      					7

#define APP_MODE						77
#define REMOTE_MODE						88
//
// GPIO Configuration
//
#ifdef	BOARD_CUSTOM

#define GPIO_TEMP_PORT       			ADC_CONFIG_PSEL_AnalogInput6	//GPIO5
#define GPIO_TEMP_ENABLE				2	//Not Used
#define GPIO_BUTTON_PORT				11
#define GPIO_VALVE_E_PORT    			6
#define GPIO_VALVE_P_PORT    			4
#define GPIO_VALVE_POW_PORT    			3
#define GPIO_LED_PORT					25
#define GPIO_BATTERY_PORT       		ADC_CONFIG_PSEL_AnalogInput3 //Not Used

#define GPIO_RED2						1
#define GPIO_GREEN2						24
#define GPIO_BLUE2						25

#endif

#ifdef QIQI_BOARD2

#define GPIO_TEMP_PORT       			ADC_CONFIG_PSEL_AnalogInput7
#define GPIO_TEMP_ENABLE				31	//Not Used
#define GPIO_BUTTON_PORT				24
#define GPIO_VALVE_E_PORT    			5
#define GPIO_VALVE_P_PORT    			4
#define GPIO_VALVE_POW_PORT    			3
#define GPIO_LED_PORT					1
#define GPIO_BATTERY_PORT       		ADC_CONFIG_PSEL_AnalogInput7 // Not Used

#endif

#ifdef QIQI_BOARD3

#define GPIO_TEMP_PORT       			ADC_CONFIG_PSEL_AnalogInput2
#define GPIO_TEMP_ENABLE				31	//Not Used
#define GPIO_BUTTON_PORT				24
#define GPIO_VALVE_E_PORT    			3
#define GPIO_VALVE_P_PORT    			2
#define GPIO_VALVE_POW_PORT    			31	// Not used
#define GPIO_LED_PORT					25
#define GPIO_BATTERY_PORT       		ADC_CONFIG_PSEL_AnalogInput7

#endif

#ifdef QIQI_BOARD4

#define GPIO_TEMP_PORT       			ADC_CONFIG_PSEL_AnalogInput2
#define GPIO_TEMP_ENABLE				2
#define GPIO_BUTTON_PORT				0
//#define GPIO_BUTTON_PORT				24
#define GPIO_VALVE_E_PORT    			5
#define GPIO_VALVE_P_PORT    			4
#define GPIO_VALVE_POW_PORT    			31	// Not used
#define GPIO_LED_PORT					27
#define GPIO_BATTERY_PORT       		ADC_CONFIG_PSEL_AnalogInput7 ///GPIO6
#endif
//
// Heat Sensor
//
//#define								MCP9700A
//#define								NOTEMPSENSOR // Define this is you want temperature rise simulated
//
//  Timer Settings
//
#define TEMP_MEASUREINTERVAL			1500
#define BATT_MEASUREINTERVAL			50
#define BUTTON_MEASUREINTERVAL			500
#define WAIT_STATE_INTERVAL				5000
#define ALARM_INTERVAL					5000

#define TEMP_ADC_SAMPLING_INTERVAL      APP_TIMER_TICKS(TEMP_MEASUREINTERVAL, APP_TIMER_PRESCALER)   //Sampling rate for the ADC reading Temperature
#define BATT_ADC_SAMPLING_INTERVAL      APP_TIMER_TICKS(BATT_MEASUREINTERVAL, APP_TIMER_PRESCALER)   //Sampling rate for the ADC reading Battery Voltage
#define BUTTON_SAMPLING_INTERVAL        APP_TIMER_TICKS(BUTTON_MEASUREINTERVAL, APP_TIMER_PRESCALER) //Sampling rate for the Button Hold
#define WAIT_STATE_SAMPLING_INTERVAL    APP_TIMER_TICKS(WAIT_STATE_INTERVAL, APP_TIMER_PRESCALER)    //Sampling rate for the Wait State
#define ALARM_SAMPLING_INTERVAL    		APP_TIMER_TICKS(ALARM_INTERVAL, APP_TIMER_PRESCALER)    	 //Sampling rate for the Auto Warmup
#define BATTERY_SAMPLING_INTERVAL		APP_TIMER_TICKS(BATT_MEASUREINTERVAL, APP_TIMER_PRESCALER)	 //Sampling rate for Battery Measurement

#define BUTTON_HOLD_TIME			    BUTTON_MEASUREINTERVAL * 4 	// 3 seconds
#define SHOWER_WAIT_LIMIT				60000 * 10 					// Turn off after 10 minutes in Wait State
#define COLDWATERTIMELIMIT				60000 * 10 					// Turn off after 10 minutes in Warmup

#define WARMUP_TIMER_PERIOD				1000						// 1 second
#define ONEDAY_IN_MILLIS				86400000
//
// ADC Modes
//
#define	TEMP_MODE						0
#define BATT_MODE						1

#define	SLOW							0
#define FAST							1
//
//  PStorage Functions
//
#define STORE_DEVICE_NAME				0x1

#define SCHED_MAX_EVENT_DATA_SIZE       sizeof(app_timer_event_t)                   /**< Maximum size of scheduler events. Note that scheduler BLE stack events do not contain any data, as the events are being pulled from the stack in the event handler. */
#define SCHED_QUEUE_SIZE                10                                          /**< Maximum number of events in the scheduler queue. */
//
// P_Storage Values
//
pstorage_module_param_t 				param;
pstorage_handle_t       				pstorage_handle;

static uint8_t 							pstorage_wait_flag = 0;		// Flags for pstorage call back
static pstorage_block_t 				pstorage_wait_handle = 0;	// Handle for pstorage call back

uint8_t  								pstorage_buffer[] = {"Master Bathroom     "};
pstorage_handle_t 						p_block_handle;
uint16_t block_num    = 0;
uint16_t block_size   = 16;
uint16_t block_offset = 0;

ble_gap_addr_t  ble_gap_addr;

static ble_gap_sec_params_t             m_sec_params;                               /**< Security requirements for this application. */
static uint16_t                         m_conn_handle = BLE_CONN_HANDLE_INVALID;    /**< Handle of the current connection. */
static ble_qiqi_t                       m_qiqi;
//
// Timers ID's
//
static app_timer_id_t                   m_adc_sampling_timer_id;
static app_timer_id_t					m_button_sampling_timer_id;
static app_timer_id_t					m_wait_state_timer_id;
static app_timer_id_t					m_alarm_timer_id;
//
// Device Manager Values
//
static dm_application_instance_t        m_app_handle;                              /**< Application identifier allocated by device manager */
static app_bond_table_t                 m_app_bond_table;
static dm_handle_t                      m_bonded_peer_handle;                       /**< Device reference handle to the current connected peer. */

static ble_uuid_t adv_uuids[] 			= {{QIQI_UUID_SERVICE, BLE_UUID_TYPE_BLE}};
//static ble_uuid_t adv_uuids[] 			= {{QIQI_UUID_SERVICE, BLE_UUID_TYPE_VENDOR_BEGIN}};

//
// Module Declarations
//
void pstorage_sys_event_handler (uint32_t p_evt); // Persistent storage system event handler
void OpenValve(void);
void CloseValve(void);
void Initialize_WarmUpVars(void);
void Enter_On_State(void);
void Enter_Ready_State(void);
void Enter_ShowerWait_State(void);
void Enter_Warmup_State(void);
void Flash_LED(int, int);

#ifdef BOARD_CUSTOM
void Flash_RGB(uint8_t count, uint8_t duration, uint8_t Temp);
#endif

void init_p_storage(void);
void clear_p_storage(void);
void read_DeviceName(void);
void store_DeviceName(void);
void ProcessButtonPush(void);
void init_uart(void);
void uart_error_handle(app_uart_evt_t*);
void SendStateAlertToApp(uint8_t, uint8_t, int);
void ConvertVoltsToFahrenheit(void);
void ComputeTemperatureVariance(void);
void MeasureBatteryVoltage(void);
void ReportBatteryVoltage(void);
char inttoASCII(int);
char* lltoa(long long val, int base);
//char* lltoa(long long val, int base);
void get_DeviceAddress(ble_gap_addr_t  *ble_gap_addr);
void Update_Shower_Cycles(void);
//
// Event Handlers
//
static void adc_sampling_timeout_handler(void * p_context);
static void button_sampling_timeout_handler(void * p_context);
static void wait_state_timeout_handler(void * p_context);
static void alarm_timeout_handler(void * p_context);
static void button_event_handler(uint8_t pin_no, uint8_t button_action);
static void app_command_handler(ble_qiqi_t * p_qiqi, uint8_t led_state);
static uint32_t device_manager_evt_handler(dm_handle_t const * p_handle, dm_event_t const  * p_event, ret_code_t event_result);
static void on_adv_evt(ble_adv_evt_t ble_adv_evt);
//
// Globals
//
int32_t adc_sample, buttonHoldTime = 0, ShowerLength = 0, ColdWaterTime = 0, ShowerWaitTimer = 0, AlarmCount = 0;
unsigned long long AppTimeInMillis, AlarmTimeInMillis, CurrentTime = 0, Wait_Start, On_Start;

uint8_t ShowerTemp = 85, ShowerSteadyStateTemperature = 93, UserSetShowerLengthMode = FALSE, AlarmMode = FALSE, adc_mode = TEMP_MODE;
uint8_t DeviceNameLoaded = FALSE, p_storage_function = 0, Advertising_Mode = FAST, precheck_Temperature = PRECHECK_TEMP, ButtonPush = HARD;
uint8_t sample_count = 0, Start_Up = 1, UserSetShowerLength = 0, Control_Mode = APP_MODE, Shower_is_Hot = SHOWERISHOT;
uint8_t warmup_count, LockOutMode = FALSE, SendShowerCycleData = FALSE, ButtonInitiatedShower = FALSE;
uint16_t sum = 0;
float variance = 0, sum1 = 0, average = 0, CurrentBatteryVoltage = 9.0;
float variance_seed[] = {41, 47, 51, 37, 34, 9, 54, 22, 12, 7}, measure[SAMPLE_TARGET];

/**@brief Function for error handling, which is called when an error has occurred.
 *
 * @warning This handler is an example only and does not fit a final product. You need to analyze
 *          how your product is supposed to react in case of error.
 *
 * @param[in] error_code  Error code supplied to the handler.
 * @param[in] line_num    Line number where the handler is called.
 * @param[in] p_file_name Pointer to the file name.
 */
void app_error_handler(uint32_t error_code, uint32_t line_num, const uint8_t * p_file_name)
{
    // This call can be used for BOARD_CUSTOM purposes during application development.
    // @note CAUTION: Activating this code will write the stack to flash on an error.
    //                This function should NOT be used in a final product.
    //                It is intended STRICTLY for development/BOARD_CUSTOMging purposes.
    //                The flash write will happen EVEN if the radio is active, thus interrupting
    //                any communication.
    //                Use with care. Un-comment the line below to use.
    //ble_BOARD_CUSTOM_assert_handler(error_code, line_num, p_file_name);

    // On assert, the system can only recover with a reset.
    //NVIC_SystemReset();
}

/**@brief Callback function for asserts in the SoftDevice.
 *
 * @details This function will be called in case of an assert in the SoftDevice.
 *
 * @warning This handler is an example only and does not fit a final product. You need to analyze
 *          how your product is supposed to react in case of Assert.
 * @warning On assert from the SoftDevice, the system can only recover on reset.
 *
 * @param[in]   line_num   Line number of the failing ASSERT call.
 * @param[in]   file_name  File name of the failing ASSERT call.
 */
void assert_nrf_callback(uint16_t line_num, const uint8_t * p_file_name)
{
    app_error_handler(DEAD_BEEF, line_num, p_file_name);
}

/**@brief Function for the LEDs initialization.
 *
 * @details Initializes all LEDs used by the application.
 */
static void gpio_init(void)
{
    // LED Init
	nrf_gpio_cfg_output(GPIO_LED_PORT);
    //
    // HBridge PortInit
    //
    nrf_gpio_cfg_output(GPIO_VALVE_E_PORT);
    nrf_gpio_cfg_output(GPIO_VALVE_P_PORT);
    nrf_gpio_cfg_output(GPIO_VALVE_POW_PORT);
    //
    // Temperature Sensor Init
    //
    nrf_gpio_cfg_output(GPIO_TEMP_ENABLE);

    #ifdef BOARD_CUSTOM	// LED Testing

    nrf_gpio_cfg_output(GPIO_RED2);
    nrf_gpio_cfg_output(GPIO_GREEN2);
    nrf_gpio_cfg_output(GPIO_BLUE2);

    #endif
    //
    // Disable
    //
    nrf_gpio_pin_clear(GPIO_VALVE_POW_PORT);
    nrf_gpio_pin_clear(GPIO_VALVE_E_PORT);
    nrf_gpio_pin_clear(GPIO_VALVE_P_PORT);
    nrf_gpio_pin_clear(GPIO_TEMP_ENABLE);
    //
    // Note: Array must be static because a pointer to it will be saved in the Button handler module.
    //
    static app_button_cfg_t buttons[] =
    {
         {GPIO_BUTTON_PORT, false, BUTTON_PULL, button_event_handler}
    };
    app_button_init(buttons, sizeof(buttons) / sizeof(buttons[0]), BUTTON_DETECTION_DELAY);

    app_button_enable();
}

static void TempSense_adc_init(void)
{
	/* Enable interrupt on ADC sample ready event*/
	NRF_ADC->INTENSET = ADC_INTENSET_END_Msk;
	sd_nvic_SetPriority(ADC_IRQn, NRF_APP_PRIORITY_LOW);
	sd_nvic_EnableIRQ(ADC_IRQn);
	//
	//  Initialize Temperature AtoD
	//
	NRF_ADC->CONFIG	= (ADC_CONFIG_EXTREFSEL_None << ADC_CONFIG_EXTREFSEL_Pos) /* Bits 17..16 : ADC external reference pin selection. */
									| (GPIO_TEMP_PORT << ADC_CONFIG_PSEL_Pos)					/*!< Use analog input 6 as analog input. */
									| (ADC_CONFIG_REFSEL_VBG << ADC_CONFIG_REFSEL_Pos)							/*!< Use internal 1.2V bandgap voltage as reference for conversion. */
									| (ADC_CONFIG_INPSEL_AnalogInputOneThirdPrescaling << ADC_CONFIG_INPSEL_Pos) /*!< Analog input specified by PSEL with no prescaling used as input for the conversion. */
									| (ADC_CONFIG_RES_10bit << ADC_CONFIG_RES_Pos);

	/*!< 10bit ADC resolution. */

	/* Enable ADC*/
	NRF_ADC->ENABLE = ADC_ENABLE_ENABLE_Enabled;
}

/**@brief Function for the Timer initialization.
 *
 * @details Initializes the timer module.
 */
static void timers_init(void)
{
	uint32_t err_code;

    // Initialize timer module, making it use the scheduler
    //APP_TIMER_APPSH_INIT(APP_TIMER_PRESCALER, APP_TIMER_MAX_TIMERS, APP_TIMER_OP_QUEUE_SIZE, true);
    APP_TIMER_APPSH_INIT(APP_TIMER_PRESCALER, APP_TIMER_MAX_TIMERS, APP_TIMER_OP_QUEUE_SIZE, false);
    //
    //  A-D/Warm-Up  Timer
    //
    err_code = app_timer_create(&m_adc_sampling_timer_id, APP_TIMER_MODE_REPEATED, adc_sampling_timeout_handler);
    APP_ERROR_CHECK(err_code);
    //
    // Button Sampling Timer
    //
    err_code = app_timer_create(&m_button_sampling_timer_id, APP_TIMER_MODE_REPEATED, button_sampling_timeout_handler);
    APP_ERROR_CHECK(err_code);
    //
    // Shower Alarm Timer
    //
    err_code = app_timer_create(&m_alarm_timer_id, APP_TIMER_MODE_REPEATED, alarm_timeout_handler);
    APP_ERROR_CHECK(err_code);
    //
    //	Wait State Timer
    //
    err_code = app_timer_create(&m_wait_state_timer_id, APP_TIMER_MODE_REPEATED, wait_state_timeout_handler);
    APP_ERROR_CHECK(err_code);
    //
    //  Battery Voltage Timer
    //
    //err_code = app_timer_create(&m_battery_test_timer_id, APP_TIMER_MODE_REPEATED, battery_voltage_timeout_handler);
    //APP_ERROR_CHECK(err_code);
}

/**@brief Function for the GAP initialization.
 *
 * @details This function sets up all the necessary GAP (Generic Access Profile) parameters of the
 *          device including the device name, appearance, and the preferred connection parameters.
 */
static void gap_params_init(void)
{
    uint32_t                err_code;
    ble_gap_conn_params_t   gap_conn_params;
    ble_gap_conn_sec_mode_t sec_mode;

    while (!DeviceNameLoaded) // Wait for Device Name to be Loaded
	{

	}
    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&sec_mode);

    if (pstorage_buffer[0] == 0xFF)
    {
    	err_code = sd_ble_gap_device_name_set(&sec_mode, (const uint8_t *)DEVICE_NAME, strlen(DEVICE_NAME));
    	APP_ERROR_CHECK(err_code);
    }
    else
    {
    	err_code = sd_ble_gap_device_name_set(&sec_mode, (const uint8_t *)pstorage_buffer, strlen((const char *)pstorage_buffer));
    	APP_ERROR_CHECK(err_code);
    }

    memset(&gap_conn_params, 0, sizeof(gap_conn_params));

    gap_conn_params.min_conn_interval = MIN_CONN_INTERVAL;
    gap_conn_params.max_conn_interval = MAX_CONN_INTERVAL;
    gap_conn_params.slave_latency     = SLAVE_LATENCY;
    gap_conn_params.conn_sup_timeout  = CONN_SUP_TIMEOUT;

    err_code = sd_ble_gap_ppcp_set(&gap_conn_params);
    APP_ERROR_CHECK(err_code);

	DeviceNameLoaded = FALSE;
/*

    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&sec_mode);

    err_code = sd_ble_gap_device_name_set(&sec_mode, (const uint8_t *)DEVICE_NAME, strlen(DEVICE_NAME));
    APP_ERROR_CHECK(err_code);

    err_code = sd_ble_gap_appearance_set(BLE_APPEARANCE_UNKNOWN);
    APP_ERROR_CHECK(err_code);

    memset(&gap_conn_params, 0, sizeof(gap_conn_params));

    gap_conn_params.min_conn_interval = MIN_CONN_INTERVAL;
    gap_conn_params.max_conn_interval = MAX_CONN_INTERVAL;
    gap_conn_params.slave_latency     = SLAVE_LATENCY;
    gap_conn_params.conn_sup_timeout  = CONN_SUP_TIMEOUT;

    err_code = sd_ble_gap_ppcp_set(&gap_conn_params);
    APP_ERROR_CHECK(err_code);
*/
}

/**@brief Function for the Device Manager initialization.
 *
 * @param[in] erase_bonds  Indicates whether bonding information should be cleared from
 *                         persistent storage during initialization of the Device Manager.
 */
static void device_manager_init(bool erase_bonds)
{
    uint32_t               err_code;
    dm_init_param_t        init_param = {.clear_persistent_data = erase_bonds};
    dm_application_param_t register_param;

    // Initialize peer device handle.
    err_code = dm_handle_initialize(&m_bonded_peer_handle);
    APP_ERROR_CHECK(err_code);

    // Initialize persistent storage module.

    err_code = pstorage_init();
    APP_ERROR_CHECK(err_code);

    err_code = dm_init(&init_param);
    APP_ERROR_CHECK(err_code);

    memset(&register_param.sec_param, 0, sizeof(ble_gap_sec_params_t));

    register_param.sec_param.bond         = SEC_PARAM_BOND;
    register_param.sec_param.mitm         = SEC_PARAM_MITM;
    register_param.sec_param.io_caps      = SEC_PARAM_IO_CAPABILITIES;
    register_param.sec_param.oob          = SEC_PARAM_OOB;
    register_param.sec_param.min_key_size = SEC_PARAM_MIN_KEY_SIZE;
    register_param.sec_param.max_key_size = SEC_PARAM_MAX_KEY_SIZE;
    register_param.evt_handler            = device_manager_evt_handler;
    register_param.service_type           = DM_PROTOCOL_CNTXT_GATT_SRVR_ID;

    err_code = dm_register(&m_app_handle, &register_param);
    APP_ERROR_CHECK(err_code);

    //app_bond_init(&m_app_bond_table);

    for(uint8_t i = 0; i < DEVICE_MANAGER_MAX_BONDS; i++)
    {
        //APP_LOG("[APP][ID: %d], Application context : %08X\r\n",m_app_bond_table.device_id[i],(unsigned int) m_app_bond_table.app_bond_cnt[i]);
    }
}

/**@brief Function for initializing the Advertising functionality.
 */
static void advertising_init(void)
{
	uint32_t      				err_code;
	ble_advdata_t 				advdata;
	ble_advdata_manuf_data_t 	adv_manuf_data;
	uint8_array_t            	adv_manuf_data_array;
	uint8_t                  	adv_manuf_data_data[2];

	//
	// Build advertising data struct to pass into @ref ble_advertising_init.
	//
	memset(&advdata, 0, sizeof(advdata));
    //
    // Configuration of manufacturer specific data
    //
    adv_manuf_data_data[0] 			= 0x1C;
    adv_manuf_data_data[1] 			= 0xC4;

    adv_manuf_data_array.p_data 	= adv_manuf_data_data;
    adv_manuf_data_array.size 		= sizeof(adv_manuf_data_data);

    adv_manuf_data.company_identifier = 0x4951; // "QI"
    adv_manuf_data.data 			= adv_manuf_data_array;

    advdata.p_manuf_specific_data 	= &adv_manuf_data;

	advdata.name_type               = BLE_ADVDATA_FULL_NAME;
	advdata.include_appearance      = true;

	advdata.flags                   = BLE_GAP_ADV_FLAGS_LE_ONLY_GENERAL_DISC_MODE;

    advdata.uuids_complete.uuid_cnt = sizeof(adv_uuids) / sizeof(adv_uuids[0]);
    advdata.uuids_complete.p_uuids  = adv_uuids;

    ble_adv_modes_config_t options =
    {
        BLE_ADV_WHITELIST_ENABLED,
        BLE_ADV_DIRECTED_DISABLED,
        BLE_ADV_DIRECTED_SLOW_DISABLED, 0,0,
        BLE_ADV_FAST_DISABLED, APP_ADV_FAST_INTERVAL, APP_ADV_FAST_TIMEOUT,
        BLE_ADV_SLOW_ENABLED, APP_ADV_SLOW_INTERVAL, APP_ADV_SLOW_TIMEOUT
    };

   	err_code = ble_advertising_init(&advdata, NULL, &options, on_adv_evt, NULL);
	APP_ERROR_CHECK(err_code);
}

/* Interrupt handler for ADC data ready event */

void ADC_IRQHandler(void)
{
	//
	// Clear data Ready event
	//
	NRF_ADC->EVENTS_END = 0;
	//
	// Read the ADC Value
	//
	adc_sample = NRF_ADC->RESULT;

	//Use the STOP task to save current. Workaround for PAN_028 rev1.5 anomaly 1.
	NRF_ADC->TASKS_STOP = 1;

	//Release the external crystal
	sd_clock_hfclk_release();

	//printf("Sample = %d\n", (int) adc_sample);

#ifdef NOTEMPSENSOR
	ShowerTemp++;
#else
	ConvertVoltsToFahrenheit();
#endif

    //Flash_RGB(2, 100, ShowerTemp);

	if (m_qiqi.SystemState == STATE_WARMUP)
		Flash_LED(2, 50);  //2 Flashes for Warmup
	//
	// Send Temperature to App
	//
	m_qiqi.ShowerTemp = ShowerTemp;

	if (warmup_count % WUT_NOTIFY_PERIOD == 0)
	{
		SendStateAlertToApp((uint8_t)ALERT_TEMPERATURE_UPDATE, (uint8_t) ALERT_TEMPERATURE_UPDATE, 2);
	}
	#ifdef BOARD_CUSTOM
		printf("Temp = %d\n", ShowerTemp);
	#endif

	if (m_qiqi.SystemState == STATE_WARMUP)
	{
		ComputeTemperatureVariance();

		warmup_count++;
	}
	else if (m_qiqi.SystemState == STATE_ON)
	{
		//ShowerSteadyStateTemperature = ShowerTemp;

		if (UserSetShowerLengthMode)
		{
			if (ShowerLength < UserSetShowerLength * 60000)
			{
				ShowerLength += TEMP_MEASUREINTERVAL;

				if (ShowerLength > ((UserSetShowerLength * 60000) - 60000))  // Warn when there is one minute left
					 Flash_LED(4, 50);
			}
			else
			{
				Enter_Ready_State();
				ShowerLength = 0;
			}
		}
		//printf("Shower Length (secs) = %lu\n", ShowerLength/1000);
	}

	if (ShowerTemp > SCALDING_TEMPERATURE)
	{
		SendStateAlertToApp((uint8_t)ALERT_SHOWER_SCALDING, (uint8_t) ALERT_SHOWER_SCALDING, 2);
	}

	//Use the STOP task to save current. Workaround for PAN_028 rev1.5 anomaly 1.
    //NRF_ADC->TASKS_STOP = 1;

	//Release the external crystal
	//sd_clock_hfclk_release();
}
//
// This Routine Not Called but exists as a consolidation for all Timer Start Commands
//

/*
static void timers_start(void)
{
    uint32_t err_code;
    //
    // ADC Timer
    //
	err_code = app_timer_start(m_adc_sampling_timer_id, TEMP_ADC_SAMPLING_INTERVAL, NULL);
	APP_ERROR_CHECK(err_code);
	//
	// Button Timer Start
	//
	err_code = app_timer_start(m_button_sampling_timer_id, BUTTON_SAMPLING_INTERVAL, NULL);
	APP_ERROR_CHECK(err_code);
	//
	// Wait State Timer
	//
	err_code = app_timer_start(m_wait_state_timer_id, WAIT_STATE_SAMPLING_INTERVAL, NULL);
	APP_ERROR_CHECK(err_code);
	//
	// Alarm Timer
	//
	err_code = app_timer_start(m_alarm_timer_id, ALARM_INTERVAL, NULL);
	APP_ERROR_CHECK(err_code);
	//
	// Battery Voltage Timer
	//
	err_code = app_timer_start(m_battery_test_timer_id, BATT_MEASUREINTERVAL, NULL);
	APP_ERROR_CHECK(err_code);
}
*/
// ADC timer handler to start ADC sampling
static void adc_sampling_timeout_handler(void * p_context)
{
	uint32_t p_is_running = 0;

	sd_clock_hfclk_request();
	while(! p_is_running) {  							//wait for the hfclk to be available
		sd_clock_hfclk_is_running((&p_is_running));
	}
	NRF_ADC->TASKS_START = 1;							//Start ADC sampling
}

/**@brief Function for handling the Device Manager events.
 *
 * @param[in] p_evt  Data associated to the device manager event.
 */
static uint32_t device_manager_evt_handler(dm_handle_t const * p_handle, dm_event_t const  * p_event, ret_code_t event_result)
{
    uint32_t err_code;

    static bool device_delete_all_started;

    // Recovery in the event of DM_DEVICE_CONTEXT_FULL
    if(event_result == DM_DEVICE_CONTEXT_FULL)
    {
        /* Clear all devices from the bond table*/
        err_code = dm_device_delete_all(&m_app_handle);
        APP_ERROR_CHECK(err_code);

        device_delete_all_started = true;
    }
    else
    {
        APP_ERROR_CHECK(event_result);
    }

    if (p_event->event_id == DM_EVT_DEVICE_CONTEXT_STORED)
    {
		#ifdef BOARD_CUSTOM
    		printf("DM: Device Context Stored\n");
		#endif
        table_index_t table_index;

        //Find first and last bond created from m_bond_index_table
        app_bond_find(&m_app_bond_table, &table_index);

    	printf("DM: Context Index: %lu\n", table_index.mr_cnt_val);

		//Increment counter if a new bond was created
        if(!(table_index.mr_cnt_val >= m_app_bond_table.app_bond_cnt[p_handle->device_id]))
        {
			#ifdef BOARD_CUSTOM
        		printf("Bond Created\n");
			#endif

           table_index.mr_cnt_val++;
           m_app_bond_table.app_bond_cnt[p_handle->device_id] = table_index.mr_cnt_val;
        }

        //Delete first created bond if bond table is full
        if(((table_index.mr_cnt_val-table_index.lr_cnt_val)== DEVICE_MANAGER_MAX_BONDS-1)
             && (table_index.lr_cnt_val != NO_APP_CONTEXT))
          {
                uint32_t err_code;
                dm_handle_t device;

                device.appl_id = 0;

                m_app_bond_table.app_bond_cnt[table_index.lr_index]=NO_APP_CONTEXT;
                device.device_id = m_app_bond_table.device_id[table_index.lr_index];

                err_code = dm_device_delete(&device);
                APP_ERROR_CHECK(err_code);
          }

        //Update the app context for new device
        app_bond_update_context(&m_app_bond_table,p_handle);

    }
    else if (p_event->event_id ==DM_EVT_DEVICE_CONTEXT_DELETED)
    {
         /* Wait for all devices to be cleared before performing a sys reset */
         if(device_delete_all_started && (p_handle->device_id == DEVICE_MANAGER_MAX_BONDS -1))
         {
             err_code = sd_nvic_SystemReset();
             APP_ERROR_CHECK(err_code);
         }
    }
    else if (p_event->event_id == DM_EVT_CONNECTION)
    {
       		//printf("DM: Device Context Loaded\n");
    }
    else if (p_event->event_id == DM_EVT_DISCONNECTION)
    {
             //printf("DM: Bond Complete!\n");
    }
    else if (p_event->event_id == DM_EVT_LINK_SECURED)
    {
           	//printf("DM: Bond Complete!\n");
    }
    else if (p_event->event_id == DM_EVT_DEVICE_CONTEXT_LOADED)
    {
    		//printf("DM: Device Context Loaded\n");
    }
    else if (p_event->event_id == DM_EVT_DEVICE_CONTEXT_LOADED)
    {
       		//printf("DM: Device Context Loaded\n");
    }
    else if (p_event->event_id ==  DM_EVT_APPL_CONTEXT_LOADED)
    {
    		//printf("DM: Application Context Loaded\n");
    }
    else if (p_event->event_id == DM_EVT_APPL_CONTEXT_STORED)
    {
        	//printf("DM: Application Context Stored\n");
    }
    else if (p_event->event_id == DM_EVT_SERVICE_CONTEXT_STORED)
    {
        	//printf("DM: Service Context Stored\n");
    }
    else if (p_event->event_id == DM_EVT_SECURITY_SETUP)
    {
            //printf("DM: Security Setup Started\n");
    }
    else if (p_event->event_id == DM_EVT_SERVICE_CONTEXT_LOADED)
    {
            //printf("DM: Security Setup Complete\n");
    }
    else if (p_event->event_id == DM_EVT_SECURITY_SETUP_COMPLETE)
    {
        	m_bonded_peer_handle = (*p_handle);
             //printf("DM: Security Setup Complete\n");
    }
    else if (p_event->event_id == DM_EVT_DEVICE_CONTEXT_STORED)
    {
        	//printf("DM: Device Context Stored\n");
    }
    else if  (p_event->event_id ==  DM_EVT_SECURITY_SETUP_REFRESH)
    {
            printf("DM: Security Setup Regresh!\n");
    }
    else if  (p_event->event_id ==  DM_EVT_LINK_SECURED)
	{
         	//printf("DM: Discovery Started!\n");
	}
    else
    {
			#ifdef BOARD_CUSTOM
    			printf("DM Event: 0x%x\n", p_event->event_id);
			#endif
    }
    return NRF_SUCCESS;
}

static void button_sampling_timeout_handler(void * p_context)
{
    uint32_t err_code;

	UNUSED_PARAMETER(p_context);

	if (!nrf_gpio_pin_read(GPIO_BUTTON_PORT))
	{
		if (buttonHoldTime > BUTTON_HOLD_TIME)
		{
			//
			// Stop Button Timer
			//
			err_code = app_timer_stop(m_button_sampling_timer_id);
			APP_ERROR_CHECK(err_code);
			buttonHoldTime = 999;

			//printf("Hold Time Exceeded\n");
			Enter_On_State();
		}
		else
		{
			buttonHoldTime +=BUTTON_MEASUREINTERVAL;
			Flash_LED(1,10);
			//printf("Hold Time\n", buttonHoldTime);
		}
	}
}

static void button_event_handler(uint8_t pin_no, uint8_t button_action)
{
    uint32_t err_code;

    switch (pin_no)
    {
        case GPIO_BUTTON_PORT:

            if (button_action)
            {
            	//
            	// Start Timer to see if this is a button hold
            	//
    			buttonHoldTime = 0;
            	err_code = app_timer_start(m_button_sampling_timer_id, BUTTON_SAMPLING_INTERVAL, NULL);
            	APP_ERROR_CHECK(err_code);
            }
        	else if (!button_action && buttonHoldTime !=  999) 	// We only care if its pressed...not un-pressed
        	{
        	  	//
        	    //  Only allow valve to open from a button push if the water is NOT scalding
        		//
        	    if (ShowerTemp < SCALDING_TEMPERATURE)
        	    {
        	    	ButtonPush = HARD;
        	    	ProcessButtonPush();
        	    }
        	    else
        	    {
            	    Flash_LED(10, 50);
        	    	Enter_Ready_State();
        	    }

        		ble_advertising_restart_without_whitelist();

        	    Flash_LED(2, 250);
    			//
    			// Stop Button Timer
    			//
    			err_code = app_timer_stop(m_button_sampling_timer_id);
    			APP_ERROR_CHECK(err_code);
    			buttonHoldTime = 0;
        	}

            break;

        default:
            APP_ERROR_HANDLER(pin_no);
            break;
    }
}

static void wait_state_timeout_handler(void * p_context)
{
    uint32_t err_code;

	UNUSED_PARAMETER(p_context);

	if (ShowerWaitTimer > SHOWER_WAIT_LIMIT)
	{
		//
		// Stop this Timer
		//
		err_code = app_timer_stop(m_wait_state_timer_id);
		APP_ERROR_CHECK(err_code);

		Flash_LED(4, 50);
		#ifdef BOARD_CUSTOM
			printf("Stop Waiting!\n");
		#endif
		ShowerWaitTimer = 0;

		Enter_Ready_State();
	}
	else
	{
		ShowerWaitTimer += WAIT_STATE_INTERVAL;

		Flash_LED(3, 50);  // 3 Flashes for Wait

		//printf("Wait\n");
	}
}

static void alarm_timeout_handler(void * p_context)
{
    //uint32_t err_code;
	//unsigned long long timeleft;

	UNUSED_PARAMETER(p_context);
	//
	//  Update Current Time
	//
	//printf("Time Update\n");

	CurrentTime += ALARM_INTERVAL;

	if (AlarmMode)
	{
		//timeleft = AlarmTimeInMillis - CurrentTime;
		//printf("Time Left: %lu hours\n", (uint32_t) timeleft/1000/60/60);

		if (CurrentTime >= AlarmTimeInMillis)
		{
			// Set New Alarm Time
			#ifdef BOARD_CUSTOM
				printf("Auto Warmup Initiatied!\n");
			#endif
			//
			// Send Alert to App
			//
			SendStateAlertToApp((uint8_t)ALERT_AUTO_WARMUP_TIME_REACHED, (uint8_t) ALERT_AUTO_WARMUP_TIME_REACHED, 2);

			AlarmTimeInMillis += ONEDAY_IN_MILLIS;

			Enter_Warmup_State();
		}
	}
}

static void app_command_handler(ble_qiqi_t * p_qiqi, uint8_t led_state)
{
	//
	// Main Command Service Routine: Respond to App Requests
	//
	uint32_t err_code;
	char char_buffer[30];
	char* TimeString;
	int8_t cmd, i, HotValue, precheckTemp;

	Flash_LED(1, 250);

	//printf("Command: %d\n", p_qiqi->AppBuffer[0]);

	cmd = p_qiqi->AppBuffer[0];

	switch (cmd)
	{
	    case CMD_SENDSTATUS: // System State Request
		  #ifdef BOARD_CUSTOM
	    	printf("System State Request\n");
		  #endif
	      SendStateAlertToApp((uint8_t)STATE_TAG, (uint8_t) m_qiqi.SystemState, 2);

	      //SEGGER_RTT_printf(0, "printf Test: %%c,         'S' : %c.\r\n", 'S');
	      break;

	    case CMD_ENTERWARMUPSTATE: // Enter WarmUp

	      Enter_Warmup_State();

	      break;

	    case CMD_ENTERWAITSTATE: // Enter Wait

	      Enter_ShowerWait_State();      //Place system in Wait State...Close the Valve

	      break;

	    case CMD_SENDTEMPTOAPP: // Send Temp to App

	      m_qiqi.ShowerTemp = ShowerTemp;

	      SendStateAlertToApp((uint8_t)ALERT_TEMPERATURE_UPDATE, (uint8_t) ALERT_TEMPERATURE_UPDATE, 2);

	      break;

	    case CMD_SENDVOLTSTOAPP: // Request Battery Measurement

	      MeasureBatteryVoltage();
	      ReportBatteryVoltage();

	      break;

	    case CMD_ENTERREADYSTATE: // Enter Ready/Standby State

	      Enter_Ready_State();

	      break;

	    case CMD_ENTERONSTATE: // Enter On State

	      Enter_On_State();

	      break;

	    case CMD_SETSHOWERLIMIT: // Set Shower Length Limit
	      //
	      //Process Shower length Request
	      //
	      if (p_qiqi->AppBuffer[2])
	      {
	          UserSetShowerLengthMode = HIGH;

	          for (i = 0; i < p_qiqi->AppBuffer[1]; i++)
	          {
	             char_buffer[i] = inttoASCII(p_qiqi->AppBuffer[i+2]);
	          }

	          char_buffer[p_qiqi->AppBuffer[1]] = '\0';

	          UserSetShowerLength = atoi(char_buffer);

	          m_qiqi.ShowerDuration = UserSetShowerLength;

	          UserSetShowerLengthMode = HIGH;
	      }
	      else  // Turn off Shower Length if data[1] == 0;
	      {
	          UserSetShowerLengthMode = LOW;
	          UserSetShowerLength = 0;
	          m_qiqi.ShowerDuration = 0;
	      }
		  #ifdef BOARD_CUSTOM
 	      	  printf("Shower Length Set: %d\n", UserSetShowerLength);
		  #endif
	      SendStateAlertToApp((uint8_t)ALERT_SHOWER_DURATION_ON, (uint8_t) ALERT_SHOWER_DURATION_ON, 2);

	      break;

	    case CMD_SOFTBUTTONPRESS: // 9 - Soft Button Press
	      //printf("Soft Button Pressed\n");

	      ButtonPush = SOFT;
	      ProcessButtonPush();  // Process Soft Button Request

	      break;

	    case CMD_SETQIQIMODE:  // App or Remote Mode

	      for (i = 0; i < p_qiqi->AppBuffer[1]; i++)
	      {
	    	    char_buffer[i] = inttoASCII(p_qiqi->AppBuffer[i+2]);
	      }

	      char_buffer[p_qiqi->AppBuffer[1]] = '\0';

	      Control_Mode = atoi(char_buffer);

	      //if (Control_Mode == APP_MODE)
	      // 	  printf("APP Mode\n");
	      //else if (Control_Mode == REMOTE_MODE)
	      //	  printf("Remote Mode\n");

	      break;

	    case CMD_GETTIMEFROMAPP: // Get Time from App

	    	//printf("Receiving Time\n");

	    	//printf("Length: %d\n ", p_qiqi->AppBuffer[1]);

	    	if (p_qiqi->AppBuffer[1] > 0)
	    	{
	    		for (i = 0; i < p_qiqi->AppBuffer[1]; i++)
	    		{
	    			char_buffer[i] = inttoASCII(p_qiqi->AppBuffer[i+2]);
	    			printf("%c",char_buffer[i]);
	    		}
				#ifdef BOARD_CUSTOM
	    			printf("\n");
				#endif
	    		char_buffer[p_qiqi->AppBuffer[1]] = '\0';

	    		AppTimeInMillis = atoll(char_buffer);

	    		CurrentTime = AppTimeInMillis;
	    		//
	    		// Start Alarm Timer
	    		//
	    		err_code = app_timer_start(m_alarm_timer_id, ALARM_SAMPLING_INTERVAL, NULL);
	    		APP_ERROR_CHECK(err_code);
	    	}

	        break;

	    case CMD_SENDTIMETOAPP: // Send Device Time to App

	    	#ifdef BOARD_CUSTOM
	    		printf("Sending Time\n");
			#endif
	    	TimeString = lltoa(CurrentTime, 10);

	    	memcpy(p_qiqi->DeviceTime, TimeString, strlen(TimeString));

	    	//printf("Length: %d\n", strlen(TimeString));
			#ifdef BOARD_CUSTOM
	    		printf("Time in Millis: %s\n", p_qiqi->DeviceTime);
			#endif
		    SendStateAlertToApp((uint8_t)ALERT_SEND_DEVICE_TIME, (uint8_t) ALERT_SEND_DEVICE_TIME, 2);

		    break;

	    case CMD_SETSHOWERONTIME: // Get Alarm Time from App

	    	if (p_qiqi->AppBuffer[1] == 0)
	    	{
	    	    //
	    	    // Stop Alarm Timer
	    	    //
	    	    err_code = app_timer_stop(m_alarm_timer_id);
	    	    APP_ERROR_CHECK(err_code);

	    	    AlarmMode = FALSE;
	    		AlarmTimeInMillis = 0;
				#ifdef BOARD_CUSTOM
	    			printf("Alarm Disabled\n");
				#endif
	    	}
	    	else
	    	{
	    		//
	    		// Get Alarm Time
	    		//
	    		for (i = 0; i < p_qiqi->AppBuffer[1]; i++)
	    		{
	    			char_buffer[i] = inttoASCII(p_qiqi->AppBuffer[i+2]);
					#ifdef BOARD_CUSTOM
	    				printf("%c",char_buffer[i]);
					#endif
	    		}
				#ifdef BOARD_CUSTOM
	    			printf("\n");
				#endif
	    		char_buffer[p_qiqi->AppBuffer[1]] = '\0';

	    		AlarmTimeInMillis = atoll(char_buffer);

	    		AlarmMode = TRUE;
				#ifdef BOARD_CUSTOM
	    			printf("Initiating Warmup in %lu hours\n", (uint32_t) (AlarmTimeInMillis - CurrentTime)/1000/60/60);
				#endif
	    	}
	        break;

	    case CMD_SETDEVICENAME:	// Set Device Broadcast Name

	        //printf("Length %d\n", p_qiqi->AppBuffer[1]);

	        for (i = 0; i < p_qiqi->AppBuffer[1]; i++)
		    {
		        pstorage_buffer[i] = p_qiqi->AppBuffer[i+2];

		        //printf("%d, %d, %c\r\n",i, p_qiqi->AppBuffer[i+2], pstorage_buffer[i]);
		    }

			#ifdef BOARD_CUSTOM
		    	//printf("\n");
			#endif
		    pstorage_buffer[p_qiqi->AppBuffer[1]] = '\0';
		    printf("Device Name Set: %s\n", pstorage_buffer);

		    //
		    //  Initiate Device Store
		    //
		    p_storage_function = STORE_DEVICE_NAME;

		    clear_p_storage();

	    	break;

	    case CMD_SENDDEVICEUID: // Send Unique ID to App

	    	get_DeviceAddress(&ble_gap_addr);

	    	memcpy(p_qiqi->DeviceAddr, ble_gap_addr.addr, BLE_GAP_ADDR_LEN);

			#ifdef BOARD_CUSTOM

	    		printf("DeviceID:");

	    		for (int a = 0; a < 6; a++)
	    	    	printf("%x", ble_gap_addr.addr[a]);

	    		printf("\n");

			#endif

	    	SendStateAlertToApp((uint8_t) ALERT_SEND_DEVICE_ADDR, (uint8_t) ALERT_SEND_DEVICE_ADDR, 2);

	    	break;

	    case CMD_SYSTEMRESET: // SystemReset

	    	NVIC_SystemReset();

	    	break;

	    case CMD_SWITCHADVMODE: // Switch Advertising Mode

	    	if (Advertising_Mode == SLOW)
	    	{
	    		Advertising_Mode = FAST;
	    	}
	    	else if (Advertising_Mode == FAST)
	    	{
	    		Advertising_Mode = SLOW;
	    	}

	        m_qiqi.AdvertisingMode = Advertising_Mode;

	    	SendStateAlertToApp((uint8_t) ALERT_SWITCH_ADV_MODE, (uint8_t) ALERT_SWITCH_ADV_MODE, 2);

	    	break;

	    case CMD_SET_PRECHECK_TEMP: // Set Precheck Temperature Value

	    	for (i = 0; i < p_qiqi->AppBuffer[1]; i++)
	    	{
	    	     char_buffer[i] = inttoASCII(p_qiqi->AppBuffer[i+2]);
	    	}

	    	char_buffer[p_qiqi->AppBuffer[1]] = '\0';

	    	precheckTemp = atoi(char_buffer);

	    	if (precheckTemp != 0)

	    		precheck_Temperature = precheckTemp;

	    	m_qiqi.PrecheckTemp = precheck_Temperature;

	    	SendStateAlertToApp((uint8_t) ALERT_PRECHECKTEMPSET,  (uint8_t) ALERT_PRECHECKTEMPSET, 2);

			#ifdef BOARD_CUSTOM
	    	      printf("Shower Precheck Value is: %d\n", precheck_Temperature);
	    	#endif

	    	break;

	    case CMD_SENDNUMSTOREDUNSHOWERS:

	    	SendStateAlertToApp((uint8_t) ALERT_SEND_NUM_STORED_SHOWERS, (uint8_t) ALERT_SEND_NUM_STORED_SHOWERS, 2);

	    	break;

	    case CMD_SENDSHOWERCYCLEDATA:

	    	SendStateAlertToApp((uint8_t) ALERT_SENDSHOWERTIMESTAMP,     (uint8_t) ALERT_SENDSHOWERTIMESTAMP, 2);

	    	SendStateAlertToApp((uint8_t) ALERT_SENDSHOWERCYCLETIMES,    (uint8_t) ALERT_SENDSHOWERCYCLETIMES, 2);

	    	break;

	    case CMD_SENDSHOWERVOLTSANDTEMP:

	    	SendStateAlertToApp((uint8_t) ALERT_SHOWERCYCLEVOLTAGE,      (uint8_t) ALERT_SHOWERCYCLEVOLTAGE, 2);

	    	SendStateAlertToApp((uint8_t) ALERT_SHOWERCYCLETEMPERATURE,  (uint8_t) ALERT_SHOWERCYCLETEMPERATURE, 2);

	    	break;

	    case CMD_SHOWERCYCLERESET:
	    	//
	    	// Initialize Shower Storage Index & Rollover Flag
	    	//
	    	m_qiqi.ShowerIndex = 0;
	    	m_qiqi.ShowerCycleRollOver = FALSE;

	    	for (i = 0; i < NUM_STORED_SHOWERS; i++)
	    		m_Shower_Cycle[i].BufferPointer = 0;

	    	break;

	    case CMD_SETSHOWERISHOTTEMP:

	    	for (i = 0; i < p_qiqi->AppBuffer[1]; i++)
	    	{
		         char_buffer[i] = inttoASCII(p_qiqi->AppBuffer[i+2]);
	    	}

	    	char_buffer[p_qiqi->AppBuffer[1]] = '\0';

	    	HotValue = atoi(char_buffer);

	    	if (HotValue != 0)

	    		Shower_is_Hot = HotValue;

	    	m_qiqi.ShowerIsHotTemp = Shower_is_Hot;

		    SendStateAlertToApp((uint8_t) ALERT_SHOWERISHOTTEMPSET,  (uint8_t) ALERT_SHOWERISHOTTEMPSET, 2);

			#ifdef BOARD_CUSTOM
	 	       printf("Shower Hot Value is: %d\n", Shower_is_Hot);
			#endif

	    	break;

	    default:

			#ifdef BOARD_CUSTOM
	    		printf("Unrecognized Command: %d\n", cmd);
			#endif

	    	break;
	  }
	  //
	  // Clear App Buffer
	  //
	  memset(p_qiqi->AppBuffer, 0, 30);

	  return;
}

void ConvertVoltsToFahrenheit()
{
	float temp2;

    temp2 = adc_sample * 3.3/1024.0;

#ifdef	MCP9700A
    temp2 = temp2 - 0.500;

    temp2 = temp2 / 0.0100;
#else
    temp2 = temp2 - 0.400;

    temp2 = temp2 / 0.0195;
#endif

    //
    // Convert to Fahrenheit
    //
    temp2 = temp2 * 1.8 + 32.0;

    ShowerTemp = (uint8_t) temp2;
}

void Flash_LED(int count, int duration)
{
    int i;

    for (i = 0; i < count; i++)
    {
    	nrf_gpio_pin_set(GPIO_LED_PORT);
        nrf_delay_ms(duration);
    	nrf_gpio_pin_clear(GPIO_LED_PORT);
        nrf_delay_ms(duration);
    }
}
#ifdef BOARD_CUSTOM

void Flash_RGB(uint8_t count, uint8_t duration, uint8_t Temp)
{
	if (Temp <= 75 )
	{
		nrf_gpio_pin_set(GPIO_BLUE2);
		nrf_delay_ms(duration);
		nrf_gpio_pin_clear(GPIO_BLUE2);
		nrf_delay_ms(duration);
	}
	else if (Temp > 75 && Temp <= 85)
	{
		nrf_gpio_pin_set(GPIO_BLUE2);
		nrf_gpio_pin_set(GPIO_RED2);

		nrf_delay_ms(duration);

		nrf_gpio_pin_clear(GPIO_BLUE2);
		nrf_gpio_pin_clear(GPIO_RED2);

		nrf_delay_ms(duration);
	}
	else if (Temp > 87)
	{
		nrf_gpio_pin_set(GPIO_RED2);
		nrf_delay_ms(duration);
		nrf_gpio_pin_clear(GPIO_RED2);
		nrf_delay_ms(duration);
	}
}

#endif

char inttoASCII(int digit)
{
  char val;

  if(digit==0)
	  val=48;
  if(digit==1)
	  val=49;
  if(digit==2)
      val=50;
  if(digit==3)
      val=51;
  if(digit==4)
      val=52;
  if(digit==5)
      val=53;
  if(digit==6)
      val=54;
  if(digit==7)
      val=55;
  if(digit==8)
      val=56;
  if(digit==9)
      val=57;

  return val;
}

char* lltoa(long long val, int base)
{
    static char buf[64] = {0};

    int i = 62;
    int sign = (val < 0);
    if(sign) val = -val;

    if(val == 0) return "0";

    for(; val && i ; --i, val /= base) {
        buf[i] = "0123456789abcdef"[val % base];
    }

    if(sign) {
        buf[i--] = '-';
    }
    return &buf[i+1];
}

/**@brief Function for initializing services that will be used by the application.
 */
static void services_init(void)
{
    uint32_t err_code;
    ble_qiqi_init_t init;

    init.app_command_handler = app_command_handler;
    err_code = ble_qiqi_init(&m_qiqi, &init);
    APP_ERROR_CHECK(err_code);
}

/**@brief Function for handling the Connection Parameters Module.
 *
 * @details This function will be called for all events in the Connection Parameters Module which
 *          are passed to the application.
 *          @note All this function does is to disconnect. This could have been done by simply
 *                setting the disconnect_on_fail config parameter, but instead we use the event
 *                handler mechanism to demonstrate its use.
 *
 * @param[in]   p_evt   Event received from the Connection Parameters Module.
 */
static void on_conn_params_evt(ble_conn_params_evt_t * p_evt)
{
    uint32_t err_code;

    if(p_evt->evt_type == BLE_CONN_PARAMS_EVT_FAILED)
    {
        err_code = sd_ble_gap_disconnect(m_conn_handle, BLE_HCI_CONN_INTERVAL_UNACCEPTABLE);
        APP_ERROR_CHECK(err_code);
    }
}

/**@brief Function for handling a Connection Parameters error.
 *
 * @param[in]   nrf_error   Error code containing information about what went wrong.
 */
static void conn_params_error_handler(uint32_t nrf_error)
{
    APP_ERROR_HANDLER(nrf_error);
}

/**@brief Function for initializing the Connection Parameters module.
 */
static void conn_params_init(void)
{
    uint32_t               err_code;
    ble_conn_params_init_t cp_init;

    memset(&cp_init, 0, sizeof(cp_init));

    cp_init.p_conn_params                  = NULL;
    cp_init.first_conn_params_update_delay = FIRST_CONN_PARAMS_UPDATE_DELAY;
    cp_init.next_conn_params_update_delay  = NEXT_CONN_PARAMS_UPDATE_DELAY;
    cp_init.max_conn_params_update_count   = MAX_CONN_PARAMS_UPDATE_COUNT;
    cp_init.start_on_notify_cccd_handle    = BLE_GATT_HANDLE_INVALID;
    cp_init.disconnect_on_fail             = false;
    cp_init.evt_handler                    = on_conn_params_evt;
    cp_init.error_handler                  = conn_params_error_handler;

    err_code = ble_conn_params_init(&cp_init);
    APP_ERROR_CHECK(err_code);
}

static void advertising_start(void)
{
    uint32_t  err_code;

    // Start advertising

    //err_code = ble_advertising_start(BLE_ADV_MODE_FAST);
    //err_code = ble_advertising_start(BLE_ADV_MODE_DIRECTED);
    //err_code = ble_advertising_start(BLE_ADV_EVT_FAST_WHITELIST);
    //err_code = ble_advertising_start(BLE_ADV_EVT_SLOW_WHITELIST);

    if (Advertising_Mode == SLOW)
    {
    	err_code = ble_advertising_start(BLE_ADV_MODE_SLOW);
    	APP_ERROR_CHECK(err_code);
    }
    else if (Advertising_Mode == FAST)
    {
    	err_code = ble_advertising_start(BLE_ADV_MODE_FAST);
    	APP_ERROR_CHECK(err_code);
    }
}

/**@brief Function for handling the Application's BLE Stack events.
 *
 * @param[in]   p_ble_evt   Bluetooth stack event.
 */
static void on_ble_evt(ble_evt_t * p_ble_evt)
{
    uint32_t                         err_code;

    //static ble_gap_evt_auth_status_t m_auth_status;
	static ble_gap_master_id_t p_master_id;
	static ble_gap_sec_keyset_t keys_exchanged;

    switch (p_ble_evt->header.evt_id)
    {
    	case BLE_GAP_ADV_FLAG_LE_LIMITED_DISC_MODE:

    		break;

        case BLE_GAP_EVT_CONNECTED:

            Flash_LED(20, 20);
        	//nrf_gpio_pin_set(CONNECTED_LED_PIN_NO);
            //nrf_gpio_pin_clear(ADVERTISING_LED_PIN_NO);
            m_conn_handle = p_ble_evt->evt.gap_evt.conn_handle;

            //err_code = app_button_enable();
            //APP_ERROR_CHECK(err_code);
			#ifdef BOARD_CUSTOM
            	printf("App Connected\n");
			#endif

            break;

        case BLE_GAP_EVT_DISCONNECTED:

        	Flash_LED(4, 250);

            m_conn_handle = BLE_CONN_HANDLE_INVALID;

			#ifdef BOARD_CUSTOM
            	printf("App disconnected\n");
			#endif
        	//ble_advertising_restart_without_whitelist();

            if (Advertising_Mode == SLOW)
            {
               	err_code = ble_advertising_start(BLE_ADV_MODE_SLOW);
               	APP_ERROR_CHECK(err_code);
            }
            else if (Advertising_Mode == FAST)
            {
               	err_code = ble_advertising_start(BLE_ADV_MODE_FAST);
               	APP_ERROR_CHECK(err_code);
            }
            //ble_advertising_start(BLE_ADV_EVT_FAST_WHITELIST);
            break;

        case BLE_GAP_EVT_SEC_PARAMS_REQUEST:
            err_code = sd_ble_gap_sec_params_reply(m_conn_handle, BLE_GAP_SEC_STATUS_SUCCESS, &m_sec_params,&keys_exchanged);
            APP_ERROR_CHECK(err_code);
            break;

        case BLE_GATTS_EVT_SYS_ATTR_MISSING:
            err_code = sd_ble_gatts_sys_attr_set(m_conn_handle, NULL, 0,BLE_GATTS_SYS_ATTR_FLAG_USR_SRVCS);
            APP_ERROR_CHECK(err_code);
            break;

        case BLE_GAP_EVT_AUTH_STATUS:
            //m_auth_status = p_ble_evt->evt.gap_evt.params.auth_status;

            break;

        case BLE_GAP_EVT_SEC_INFO_REQUEST:

            //p_enc_info = keys_exchanged.keys_central.p_enc_key;

            if (p_master_id.ediv == p_ble_evt->evt.gap_evt.params.sec_info_request.master_id.ediv)
            {
                err_code = sd_ble_gap_sec_info_reply(m_conn_handle, &keys_exchanged.keys_central.p_enc_key->enc_info, &keys_exchanged.keys_central.p_id_key->id_info, NULL);
                APP_ERROR_CHECK(err_code);
				p_master_id.ediv = p_ble_evt->evt.gap_evt.params.sec_info_request.master_id.ediv;
            }
            else
            {
                // No keys found for this device
                err_code = sd_ble_gap_sec_info_reply(m_conn_handle, NULL, NULL,NULL);
                APP_ERROR_CHECK(err_code);
            }
            break;

        case BLE_GAP_EVT_TIMEOUT:

        	if (p_ble_evt->evt.gap_evt.params.timeout.src == BLE_GAP_TIMEOUT_SRC_ADVERTISING)
            {
                //nrf_gpio_pin_clear(ADVERTISING_LED_PIN_NO);

                // Configure buttons with sense level low as wakeup source.
                //nrf_gpio_cfg_sense_input(GPIO_BUTTON_PORT,
                //                         BUTTON_PULL,
                //                         NRF_GPIO_PIN_SENSE_LOW);

                // Go to system-off mode (this function will not return; wakeup will cause a reset)
                //err_code = sd_power_system_off();
                //APP_ERROR_CHECK(err_code);
                //printf("EVT Timeout\n");
                advertising_start();
            }
            break;

        case BLE_GAP_AD_TYPE_SLAVE_CONNECTION_INTERVAL_RANGE:

        	break;

        case BLE_EVT_USER_MEM_REQUEST:
             err_code = sd_ble_user_mem_reply(m_conn_handle, NULL);
             APP_ERROR_CHECK(err_code);
             break;

        default:
            // No implementation needed.
            //printf("Uncaught event: %d\n", p_ble_evt->header.evt_id);
            break;
    }
}

/**@brief Function for handling advertising events.
 *
 * @details This function will be called for advertising events which are passed to the application.
 *
 * @param[in] ble_adv_evt  Advertising event.
 */
static void on_adv_evt(ble_adv_evt_t ble_adv_evt)
{
    uint32_t err_code;

	//printf("Advertising Event: %ul\n", ble_adv_evt);

    switch (ble_adv_evt)
    {
        case BLE_ADV_EVT_FAST:
            //err_code = bsp_indication_set(BSP_INDICATE_ADVERTISING);
            //APP_ERROR_CHECK(err_code);
            break;

        case BLE_ADV_EVT_SLOW:

        	//printf("Advertising Slow.\n");
        	break;

        case BLE_ADV_EVT_IDLE:
            //sleep_mode_enter();
         	//printf("Advertising Idle.\n");
            break;

        case BLE_ADV_EVT_DIRECTED:
         	//printf("Advertising Directed.\n");
        	break;

        case BLE_ADV_EVT_DIRECTED_SLOW:
			#ifdef _BOARD_CUSTOM
         		printf("Advertising Directed Slow. \n");
			#endif

         	break;

        case BLE_ADV_EVT_FAST_WHITELIST:
			#ifdef _BOARD_CUSTOM
         		printf("Advertising Fast Whitelist. \n");
			#endif

         	break;

        case BLE_ADV_EVT_SLOW_WHITELIST:
			#ifdef _BOARD_CUSTOM
        		printf("Advertising Slow Whitelist. \n");
			#endif
        	break;

        case BLE_ADV_EVT_WHITELIST_REQUEST:
        {
        	//printf("Creating Whitelist\n");

            ble_gap_whitelist_t 	whitelist;
            ble_gap_addr_t    * 	p_whitelist_addr[BLE_GAP_WHITELIST_ADDR_MAX_COUNT];
            ble_gap_irk_t     * 	p_whitelist_irk[BLE_GAP_WHITELIST_IRK_MAX_COUNT];

            whitelist.addr_count = 	BLE_GAP_WHITELIST_ADDR_MAX_COUNT;
            whitelist.irk_count  = 	BLE_GAP_WHITELIST_IRK_MAX_COUNT;
            whitelist.pp_addrs   = 	p_whitelist_addr;
            whitelist.pp_irks    = 	p_whitelist_irk;

            err_code = dm_whitelist_create(&m_app_handle, &whitelist);
            APP_ERROR_CHECK(err_code);

            err_code = ble_advertising_whitelist_reply(&whitelist);
            APP_ERROR_CHECK(err_code);
            break;
        }
        case BLE_ADV_EVT_PEER_ADDR_REQUEST:
        {
        	//printf("Advertising Peer Request. \n");

            ble_gap_addr_t peer_address;

            // Only Give peer address if we have a handle to the bonded peer.
            if (m_bonded_peer_handle.appl_id != DM_INVALID_ID)
            {
                err_code = dm_peer_addr_get(&m_bonded_peer_handle, &peer_address);
                APP_ERROR_CHECK(err_code);

                err_code = ble_advertising_peer_addr_reply(&peer_address);
                APP_ERROR_CHECK(err_code);
            }
        }
            break;

        default:
            break;
    }
}

/**@brief Function for dispatching a BLE stack event to all modules with a BLE stack event handler.
 *
 * @details This function is called from the scheduler in the main loop after a BLE stack
 *          event has been received.
 *
 * @param[in]   p_ble_evt   Bluetooth stack event.
 */
static void ble_evt_dispatch(ble_evt_t * p_ble_evt)
{
    dm_ble_evt_handler(p_ble_evt);
    on_ble_evt(p_ble_evt);
    ble_advertising_on_ble_evt(p_ble_evt);
    ble_conn_params_on_ble_evt(p_ble_evt);
    ble_qiqi_on_ble_evt(&m_qiqi, p_ble_evt);
}

/**@brief Function for dispatching a system event to interested modules.
 *
 * @details This function is called from the System event interrupt handler after a system
 *          event has been received.
 *
 * @param[in]   sys_evt   System stack event.
 */
static void sys_evt_dispatch(uint32_t sys_evt)
{
	/* if ((sys_evt == NRF_EVT_FLASH_OPERATION_SUCCESS) || (sys_evt == NRF_EVT_FLASH_OPERATION_ERROR))
	{
		pstorage_sys_event_handler(sys_evt);
	}*/
    pstorage_sys_event_handler(sys_evt);
    ble_advertising_on_sys_evt(sys_evt);
}

/**@brief Function for initializing the BLE stack.
 *
 * @details Initializes the SoftDevice and the BLE event interrupt.
 */
static void ble_stack_init(void)
{
    uint32_t err_code;

    	// Initialize the SoftDevice handler module.
#ifdef BOARD_CUSTOM
    SOFTDEVICE_HANDLER_INIT(NRF_CLOCK_LFCLKSRC_XTAL_20_PPM, false);
#else
    SOFTDEVICE_HANDLER_INIT(NRF_CLOCK_LFCLKSRC_RC_250_PPM_4000MS_CALIBRATION, false);
#endif
        // Enable BLE stack.
        ble_enable_params_t ble_enable_params;
        memset(&ble_enable_params, 0, sizeof(ble_enable_params));
    #ifdef S130
        ble_enable_params.gatts_enable_params.attr_tab_size   = BLE_GATTS_ATTR_TAB_SIZE_DEFAULT;
    #endif
        ble_enable_params.gatts_enable_params.service_changed = IS_SRVC_CHANGED_CHARACT_PRESENT;
        err_code = sd_ble_enable(&ble_enable_params);
        APP_ERROR_CHECK(err_code);

        // Register with the SoftDevice handler module for BLE events.
        err_code = softdevice_ble_evt_handler_set(ble_evt_dispatch);
        APP_ERROR_CHECK(err_code);

        // Register with the SoftDevice handler module for BLE events.
        err_code = softdevice_sys_evt_handler_set(sys_evt_dispatch);
        APP_ERROR_CHECK(err_code);
}

void ProcessButtonPush(void)
{
    	//uint32_t	err_code;

        if (m_qiqi.SystemState == STATE_READY)
        {
            if (CurrentBatteryVoltage < (float) BATTERY_CRITICAL)
            {
            	m_qiqi.SystemState = STATE_WARMUP;
            	SendStateAlertToApp((uint8_t)STATE_TAG, (uint8_t) ALERT_DEVICE_ENTERED_WARMUP_STATE, 2);

            	LockOutMode = TRUE;
            	OpenValve();
            }
            else
            {
                Enter_Warmup_State();

                Initialize_WarmUpVars();

                if (ButtonPush == HARD)
                	ButtonInitiatedShower = TRUE;
                else
                	ButtonInitiatedShower = FALSE;
            }
        }
        else if (m_qiqi.SystemState == STATE_WAIT)
        {
            Enter_On_State();
        }
        else if (m_qiqi.SystemState == STATE_ON)
        {
            Enter_Ready_State();
        }
        else if (m_qiqi.SystemState == STATE_WARMUP)
        {
        	if (LockOutMode == FALSE)
        		Enter_Ready_State();
        }
        //
        // Update Shower Cycle Buffer
        //
    	if (ButtonPush == HARD && CurrentTime != 0 && ButtonInitiatedShower == TRUE)
    		Update_Shower_Cycles();
}

/**@brief Function for the Power manager.
 */
static void power_manage(void)
{
    uint32_t err_code = sd_app_evt_wait();
    APP_ERROR_CHECK(err_code);
}

void SendStateAlertToApp(uint8_t tag, uint8_t AlertVal, int message_length)
{
    uint32_t err_code;

    if (Control_Mode == APP_MODE)
    {
    	err_code = ble_transmit_data_to_app(&m_qiqi, tag);

    	if 	(err_code != NRF_SUCCESS &&	err_code != BLE_ERROR_INVALID_CONN_HANDLE && err_code != NRF_ERROR_INVALID_STATE)
    	{
    		APP_ERROR_CHECK(err_code);
    	}
    }
}

void ComputeTemperatureVariance(void)
{
	uint8_t i, j, precheck;

	precheck = ShowerTemp;

#ifdef	BOARD_CUSTOM
    Flash_RGB(2, 100, ShowerTemp);
#endif
	if (precheck > precheck_Temperature)
	{
		measure[sample_count] = (float) ShowerTemp;

		for (j = 0; j < SAMPLE_TARGET; j++)
		{
			sum = sum + measure[j];

		    //printf("Measure: %f\n", measure[j]);
		}

		//printf("Sum: %d\n", sum);

		average = sum / (float) SAMPLE_TARGET;

		//printf("Average: %f\n", average);

		for (i = 0; i < SAMPLE_TARGET; i++)
		{
			//sum1 = sum1 + pow( (float) (measure[i] - average), 2);

			sum1 = sum1 + ( measure[i] - average ) *  ( measure[i] - average );
		}

		//printf("sum1: %f\n", sum1);

		variance =  sum1 /(float) (SAMPLE_TARGET);

		//printf("Variance: %f\n", variance);

		if (variance < (float) VARIANCE_TARGET  || average >= Shower_is_Hot)
		{
			Initialize_WarmUpVars();
			#ifdef BOARD_CUSTOM
				printf("Converged!\n");
			#endif
			Enter_ShowerWait_State();

#ifdef NOTEMPSENSOR
			ShowerTemp = 85;
#endif
			sample_count = 0;
		}
		else
		{
			sample_count++;

			if (sample_count == SAMPLE_TARGET)
			{
				sample_count = 0;
			}
			sum = 0; sum1 = 0;
		}
	}
	else
	{
		if (warmup_count % WUT_NOTIFY_PERIOD == 0)
		{
			#ifdef BOARD_CUSTOM
				printf("Cold Water\n");
			#endif
		}

	    ColdWaterTime = ColdWaterTime + WARMUP_TIMER_PERIOD;

	    if (ColdWaterTime > COLDWATERTIMELIMIT)
	    {
	    	SendStateAlertToApp((uint8_t)STATE_TAG, (uint8_t) ALERT_COLD_WATER_ABORT, 2);
			Initialize_WarmUpVars();
			#ifdef BOARD_CUSTOM
		    	printf("Cold Water Abort!!\n");
			#endif
	    	Enter_Ready_State();
	    }
	}
}

void Initialize_WarmUpVars(void)
{
    int k;
	sample_count = 0, sum = 0, sum1 = 0;
    variance = 0, average = 0; ColdWaterTime = 0;

    for (k = 0; k < SAMPLE_TARGET; k++)
    {
        	measure[k] = variance_seed[k];
    }
}

#ifdef BOARD_CUSTOM

void OpenValve(void)
{
	nrf_gpio_pin_set(GPIO_VALVE_POW_PORT);
    nrf_delay_ms(100);

	nrf_gpio_pin_clear(GPIO_VALVE_P_PORT);
	nrf_gpio_pin_set(GPIO_VALVE_E_PORT);
    nrf_delay_ms(40);

    nrf_gpio_pin_clear(GPIO_VALVE_E_PORT);
    nrf_delay_ms(100);

    nrf_gpio_pin_clear(GPIO_VALVE_POW_PORT);
}

void CloseValve(void)
{
	nrf_gpio_pin_set(GPIO_VALVE_POW_PORT);
    nrf_delay_ms(100);

	nrf_gpio_pin_set(GPIO_VALVE_P_PORT);
	nrf_gpio_pin_set(GPIO_VALVE_E_PORT);
    nrf_delay_ms(40);

    nrf_gpio_pin_clear(GPIO_VALVE_E_PORT);
    nrf_delay_ms(100);

    nrf_gpio_pin_clear(GPIO_VALVE_POW_PORT);
}

#endif

#ifdef QIQI_BOARD2

void OpenValve(void)
{

}

void CloseValve(void)
{

}

#endif

#if defined(QIQI_BOARD3) || defined(QIQI_BOARD4)

void OpenValve(void)
{
	nrf_gpio_pin_set(GPIO_VALVE_E_PORT);

	nrf_delay_ms(40);
	//
	// Check Battery Voltage During Actuation
	//
	MeasureBatteryVoltage();

	ReportBatteryVoltage();

	nrf_delay_ms(10);

    nrf_gpio_pin_clear(GPIO_VALVE_E_PORT);
}

void CloseValve(void)
{
	nrf_gpio_pin_set(GPIO_VALVE_P_PORT);

	nrf_delay_ms(50);

	nrf_gpio_pin_clear(GPIO_VALVE_P_PORT);
}

#endif

void MeasureBatteryVoltage(void)
{
    //uint32_t err_code;

    // interrupt ADC
    NRF_ADC->INTENSET = (ADC_INTENSET_END_Disabled << ADC_INTENSET_END_Pos);  /*!< Interrupt enabled. */

    // config ADC
    NRF_ADC->CONFIG	= (ADC_CONFIG_EXTREFSEL_None << ADC_CONFIG_EXTREFSEL_Pos) /* Bits 17..16 : ADC external reference pin selection. */
    				| (GPIO_BATTERY_PORT << ADC_CONFIG_PSEL_Pos)			  /*!< Use analog input 0 as analog input. */
    				| (ADC_CONFIG_REFSEL_VBG << ADC_CONFIG_REFSEL_Pos)		  /*!< Use internal 1.2V bandgap voltage as reference for conversion. */
    				| (ADC_CONFIG_INPSEL_AnalogInputOneThirdPrescaling << ADC_CONFIG_INPSEL_Pos) /*!< Analog input specified by PSEL with no prescaling used as input for the conversion. */
    				| (ADC_CONFIG_RES_10bit << ADC_CONFIG_RES_Pos);			  /*!< 10bit ADC resolution. */

    // enable ADC
    NRF_ADC->ENABLE = ADC_ENABLE_ENABLE_Enabled;					 		  /* Bit 0 : ADC enable. */

    // start ADC conversion
    NRF_ADC->TASKS_START = 1;

    // wait for conversion to end
    while (!NRF_ADC->EVENTS_END)
    {

    }
    NRF_ADC->EVENTS_END	= 0;

   	//Save your ADC result
   	adc_sample = NRF_ADC->RESULT;

   	//CurrentBatteryVoltage = (float) adc_sample * 1.17 * 3.6/1024.0 * 11.8; //Steve

   	CurrentBatteryVoltage = (float) adc_sample * 1.17 * 3.6/1024.0 * 11.8;	//EQ

   	//Use the STOP task to save current. Workaround for PAN_028 rev1.1 anomaly 1.
    NRF_ADC->TASKS_STOP = 1;
    //
    // Configure back to Measure Temperature
    //
    NRF_ADC->INTENSET = (ADC_INTENSET_END_Disabled << ADC_INTENSET_END_Pos);  /*!< Interrupt enabled. */

	NRF_ADC->CONFIG	= (ADC_CONFIG_EXTREFSEL_None << ADC_CONFIG_EXTREFSEL_Pos) /* Bits 17..16 : ADC external reference pin selection. */
									| (GPIO_TEMP_PORT << ADC_CONFIG_PSEL_Pos)					/*!< Use analog input 6 as analog input. */
									| (ADC_CONFIG_REFSEL_VBG << ADC_CONFIG_REFSEL_Pos)							/*!< Use internal 1.2V bandgap voltage as reference for conversion. */
									| (ADC_CONFIG_INPSEL_AnalogInputOneThirdPrescaling << ADC_CONFIG_INPSEL_Pos) /*!< Analog input specified by PSEL with no prescaling used as input for the conversion. */
									| (ADC_CONFIG_RES_10bit << ADC_CONFIG_RES_Pos);

	/* Enable ADC*/
	NRF_ADC->ENABLE = ADC_ENABLE_ENABLE_Enabled;
	// start ADC conversion
	NRF_ADC->TASKS_START = 1;

	// wait for conversion to end
	while (!NRF_ADC->EVENTS_END)
	{}
	NRF_ADC->EVENTS_END	= 0;

	//Use the STOP task to save current.
	NRF_ADC->TASKS_STOP = 1;
}

void ReportBatteryVoltage(void)
{
    char str[10];

    sprintf(str, "%.3f", CurrentBatteryVoltage);

    memcpy(m_qiqi.BatteryVoltage, str, strlen(str));

    if (CurrentBatteryVoltage > (float) BATTERY_LOW)	// Battery OK
    {
    	SendStateAlertToApp((uint8_t)ALERT_DEVICE_VOLTAGE_UPDATE, (uint8_t) ALERT_DEVICE_VOLTAGE_UPDATE, 2);
    }
    else if (CurrentBatteryVoltage < (float) BATTERY_LOW && CurrentBatteryVoltage > (float) BATTERY_CRITICAL)	//Battery Low
    {
    	SendStateAlertToApp((uint8_t)ALERT_BATTERY_LEVEL_LOW_UPDATE, (uint8_t) ALERT_BATTERY_LEVEL_LOW_UPDATE, 2);
    }
    else if (CurrentBatteryVoltage < (float) BATTERY_LOW && CurrentBatteryVoltage < (float) BATTERY_CRITICAL)	//Battery Level Critical
    {
    	SendStateAlertToApp((uint8_t)ALERT_BATTERY_LEVEL_CRITICAL, (uint8_t) ALERT_BATTERY_LEVEL_CRITICAL, 2);
    }
}

void Update_Shower_Cycles(void)
{
	   char* WarmupString;
	   char CharBuffer[20];
	   uint8_t	WString;

	   switch (m_qiqi.SystemState)
	   {
	    	case STATE_WARMUP:

	    		m_Shower_Cycle[m_qiqi.ShowerIndex].Warmup_Start = CurrentTime;
	    	    //
	    	    // Add WarmupTime Start Time/Date to Buffer
	    	    //
	    	    WarmupString = lltoa(m_Shower_Cycle[m_qiqi.ShowerIndex].Warmup_Start, 10);

	    	    WString = strlen(WarmupString);

	    	    memcpy(&m_Shower_Cycle[m_qiqi.ShowerIndex].Warmup_Start_ASCII, WarmupString, WString);

	    	    m_Shower_Cycle[m_qiqi.ShowerIndex].WarmupASCII_Length = WString;

	    		break;

	    	case STATE_WAIT:

	    		m_Shower_Cycle[m_qiqi.ShowerIndex].Warmup_Time = (uint16_t) (CurrentTime - m_Shower_Cycle[m_qiqi.ShowerIndex].Warmup_Start)/1000;

	      		//m_Shower_Cycle[m_qiqi.ShowerIndex].Warmup_Time = (uint16_t) 2048; //DEBUGGGGGG
	    	    //
	    	    // Add Warmup Time to Buffer
	    	    //
	    	    sprintf(CharBuffer, "%d", m_Shower_Cycle[m_qiqi.ShowerIndex].Warmup_Time);

	    	    ble_update_ShowerCycles(CharBuffer, &m_qiqi);

	    		Wait_Start  = CurrentTime;

	    		break;

	    	case STATE_ON:

	    		m_Shower_Cycle[m_qiqi.ShowerIndex].Wait_Time = (uint16_t) (CurrentTime - Wait_Start)/1000;

	      		//m_Shower_Cycle[m_qiqi.ShowerIndex].Wait_Time = (uint16_t) 12345; //DEBUGGGGGG
	    	    //
	    	    // Add Wait Time to Buffer
	    	    //
	    	    sprintf(CharBuffer, "%d", m_Shower_Cycle[m_qiqi.ShowerIndex].Wait_Time);

	    	    ble_update_ShowerCycles(CharBuffer, &m_qiqi);

	    		On_Start  = CurrentTime;

	    		break;

	    	case STATE_READY:

	    		if (m_Shower_Cycle[m_qiqi.ShowerIndex].BufferPointer != 0)
	    		{
					m_Shower_Cycle[m_qiqi.ShowerIndex].On_Time = (uint16_t) (CurrentTime - On_Start)/1000;

					//m_Shower_Cycle[m_qiqi.ShowerIndex].On_Time = (uint16_t) 55555; //DEBUGGGGGG
					//
					// Add On Time, Temperature, & Voltage to Buffer
					//
					sprintf(CharBuffer, "%d", m_Shower_Cycle[m_qiqi.ShowerIndex].On_Time);

					ble_update_ShowerCycles(CharBuffer, &m_qiqi);
					//
					// After we write the On Time, save the buffer length and reset Buffer Pointer
					//
					m_Shower_Cycle[m_qiqi.ShowerIndex].BufferLength = m_Shower_Cycle[m_qiqi.ShowerIndex].BufferPointer;

					m_Shower_Cycle[m_qiqi.ShowerIndex].BufferPointer = 0;

					//
					// Save Voltage & Temperature
					//
					memcpy(&m_Shower_Cycle[m_qiqi.ShowerIndex].CycleVoltage, &m_qiqi.BatteryVoltage, strlen((const char*)m_qiqi.BatteryVoltage));

					m_Shower_Cycle[m_qiqi.ShowerIndex].CycleTemp = ShowerTemp;

					if (m_qiqi.ShowerIndex < NUM_STORED_SHOWERS)
					{
						m_qiqi.ShowerIndex++;
					}
					else
					{
						m_qiqi.ShowerIndex 			= 0;
						m_qiqi.ShowerCycleRollOver 	= TRUE;
					}
	    		}

	    		break;

	    	 default:

	    		break;
	   }
}

void Enter_Warmup_State(void)
{
    uint32_t err_code;

    #ifdef BOARD_CUSTOM
    	printf("Entering WarmUp\n");
	#endif
    //
    // Enable Temp Sensor
    //
    nrf_gpio_pin_set(GPIO_TEMP_ENABLE);

    OpenValve();
    m_qiqi.SystemState = STATE_WARMUP;

    //Initialize Sample Counter
    sample_count = 0;  warmup_count = 0;
    //
    // Start Warmup Timer and begin measuring temperature
    //
	err_code = app_timer_start(m_adc_sampling_timer_id, TEMP_ADC_SAMPLING_INTERVAL, NULL);
	APP_ERROR_CHECK(err_code);

    SendStateAlertToApp((uint8_t)STATE_TAG, (uint8_t) ALERT_DEVICE_ENTERED_WARMUP_STATE, 2);
}

void Enter_ShowerWait_State(void)
{
    uint32_t err_code;

    #ifdef BOARD_CUSTOM
    	printf("Entering Wait State\n");
	#endif
/*
    //
    // Stop Warmup Timer
    //
    err_code = app_timer_stop(m_adc_sampling_timer_id);
    APP_ERROR_CHECK(err_code);
    //
    // Disable Temp Sensor
    //
    nrf_gpio_pin_clear(GPIO_TEMP_ENABLE);
 */
    //
    // Start Wait State Timer
    //
	err_code = app_timer_start(m_wait_state_timer_id, WAIT_STATE_SAMPLING_INTERVAL, NULL);
	APP_ERROR_CHECK(err_code);

	CloseValve();
    m_qiqi.SystemState = STATE_WAIT;

    SendStateAlertToApp((uint8_t)STATE_TAG, (uint8_t) ALERT_DEVICE_ENTERED_WAIT_STATE, 2);
    //
    // Update Shower Cycle Buffer
    //
    if (ButtonPush == HARD)
    	Update_Shower_Cycles();
}

void Enter_On_State(void)
{
    uint32_t err_code;

	#ifdef BOARD_CUSTOM
    	printf("Entering On State\n");
	#endif

    OpenValve();
    m_qiqi.SystemState = STATE_ON;
/*
    //
    // Enable Temp Sensor
    //
    nrf_gpio_pin_set(GPIO_TEMP_ENABLE);
    //
    // Start Timer to Measure Temperature during Shower
    //
	err_code = app_timer_start(m_adc_sampling_timer_id, TEMP_ADC_SAMPLING_INTERVAL, NULL);
    APP_ERROR_CHECK(err_code);
*/
    //
    // Stop Wait State Timer
    //
    err_code = app_timer_stop(m_wait_state_timer_id);
    APP_ERROR_CHECK(err_code);

    SendStateAlertToApp((uint8_t)STATE_TAG, (uint8_t) ALERT_DEVICE_ENTERED_ON_STATE, 2);

	ShowerWaitTimer = 0;
}

void Enter_Ready_State(void)
{
    uint32_t err_code;
    //
    // Stop Warmup Timer
    //
    err_code = app_timer_stop(m_adc_sampling_timer_id);
    APP_ERROR_CHECK(err_code);

	nrf_delay_ms(500);

    #ifdef BOARD_CUSTOM
    	printf("Entering Ready State\n");
	#endif
    //
    // Disable Temp Sensor
    //
    nrf_gpio_pin_clear(GPIO_TEMP_ENABLE);

    CloseValve();
    m_qiqi.SystemState = STATE_READY;

    ShowerLength = 0;

	ShowerWaitTimer = 0;

    SendStateAlertToApp((uint8_t)STATE_TAG, (uint8_t) ALERT_DEVICE_ENTERED_READY_STATE, 2);
}

#ifdef BOARD_CUSTOM
void init_uart(void)
{
    uint32_t err_code;
    const app_uart_comm_params_t comm_params =
    {
          RX_PIN_NUMBER,
          TX_PIN_NUMBER,
          RTS_PIN_NUMBER,
          CTS_PIN_NUMBER,
          APP_UART_FLOW_CONTROL_DISABLED,
          false,
          UART_BAUDRATE_BAUDRATE_Baud38400
    };

    APP_UART_FIFO_INIT(&comm_params,
                        UART_RX_BUF_SIZE,
                        UART_TX_BUF_SIZE,
                        uart_error_handle,
                        APP_IRQ_PRIORITY_LOW,
                        err_code);

     APP_ERROR_CHECK(err_code);
}

void uart_error_handle(app_uart_evt_t * p_event)
{
    if (p_event->evt_type == APP_UART_COMMUNICATION_ERROR)
    {
        APP_ERROR_HANDLER(p_event->data.error_communication);
    }
    else if (p_event->evt_type == APP_UART_FIFO_ERROR)
    {
        APP_ERROR_HANDLER(p_event->data.error_code);
    }
}
#endif

void get_DeviceAddress(ble_gap_addr_t  *ble_gap_addr)
{
    memset(ble_gap_addr, 0, sizeof(&ble_gap_addr));
    ble_gap_addr->addr_type= BLE_GAP_ADDR_TYPE_RANDOM_STATIC;//BLE_GAP_ADDR_TYPE_PUBLIC;
    sd_ble_gap_address_get(ble_gap_addr);
    return;
}

static void pstorage_cb_handler(pstorage_handle_t * handle, uint8_t op_code, uint32_t result, uint8_t * p_data, uint32_t data_len)
{
		if (handle->block_id == pstorage_wait_handle) { pstorage_wait_flag = 0; }  //If we are waiting for this callback, clear the wait flag.

		switch(op_code)
		{
			case PSTORAGE_LOAD_OP_CODE:

				 if (result == NRF_SUCCESS)
				 {
				 	 	 DeviceNameLoaded = TRUE;

						 if (pstorage_buffer[0] != 0xFF)
						 {
						 	 #ifdef BOARD_CUSTOM
							 	 printf("Device Name: %s\n", pstorage_buffer);
						 	 #endif
						 }
						 //printf("pstorage LOAD callback received \r\n");
						 //bsp_indication_set(BSP_INDICATE_ALERT_0);
				 }
				 else
				 {
						 //printf("pstorage LOAD ERROR callback received \r\n");
						 //bsp_indication_set(BSP_INDICATE_RCV_ERROR);
				 }

				 break;

			case PSTORAGE_STORE_OP_CODE:

				if (result == NRF_SUCCESS)
				 {
						 printf("pstorage STORE callback received \r\n");
						 //bsp_indication_set(BSP_INDICATE_ALERT_1);
						 //pstorage_load(pstorage_buffer, &p_block_handle, 16, 0);
						 NVIC_SystemReset();
				 }
				 else
				 {
					   	 //printf("pstorage STORE ERROR callback received \r\n");
						 //bsp_indication_set(BSP_INDICATE_RCV_ERROR);
				 }
				 break;

			case PSTORAGE_UPDATE_OP_CODE:

				 if (result == NRF_SUCCESS)
				 {
						 //printf("pstorage UPDATE callback received \r\n");
						 //bsp_indication_set(BSP_INDICATE_ALERT_2);
				 }
				 else
				 {
						 //printf("pstorage UPDATE ERROR callback received \r\n");
						 //bsp_indication_set(BSP_INDICATE_RCV_ERROR);
				 }
				 break;

			case PSTORAGE_CLEAR_OP_CODE:

				 if (result == NRF_SUCCESS)
				 {
					 printf("pstorage CLEAR callback received \r\n");
					 //bsp_indication_set(BSP_INDICATE_ALERT_3);
					 if (p_storage_function == STORE_DEVICE_NAME)
					 {
							store_DeviceName();
					 }
				 }
				 else
				 {
					 	 //printf("pstorage CLEAR ERROR callback received \r\n");
						 //bsp_indication_set(BSP_INDICATE_RCV_ERROR);
				 }
				 break;
/*
			case INVALID_OP_CODE:
						printf("pstorage ERROR callback received \r\n");
						bsp_indication_set(BSP_INDICATE_RCV_ERROR);
				 break;
*/
		}
}

void store_DeviceName(void)
{
	uint32_t err_code;

	err_code = pstorage_store(&p_block_handle, pstorage_buffer, block_size, block_offset);
	if (err_code != NRF_SUCCESS)
	{
    	//printf("pstorage Store Error!\r\n");
	}
}

//
// Initiate Read of Device Name from pstorage
//
void read_DeviceName(void)
{
	uint32_t err_code;

	err_code = pstorage_load(pstorage_buffer, &p_block_handle, block_size, block_offset);

	if (err_code != NRF_SUCCESS)
	{
	    //printf("pstorage Read Error!\r\n");
	}
}

void clear_p_storage(void)
{
	uint32_t err_code;

	err_code =	pstorage_clear(&p_block_handle, block_size);

	if (err_code != NRF_SUCCESS)
	{
	    //printf("pstorage Read Error!\r\n");
	}
	//printf("pstorage clear operation requested... \r\n");
}

void init_p_storage(void)
{
	uint32_t                	retval;
	pstorage_module_param_t 	param;

	param.block_size  = 16;                    //Select block size of 16 bytes
	param.block_count = 4;                     //Select 4 blocks, total of 64 bytes
	param.cb          = pstorage_cb_handler;   //Set the pstorage callback handler
	//
	// Register for pstorage
	//
	retval = pstorage_register(&param, &pstorage_handle);
	if (retval != NRF_SUCCESS)
	{
		printf("p_storage registration error\r\n");
		//bsp_indication_set(BSP_INDICATE_FATAL_ERROR);
	}
	//
	//  Get the Block Handle
	//
	retval = pstorage_block_identifier_get(&pstorage_handle, block_num, &p_block_handle);

	APP_ERROR_CHECK(retval);
	if (retval != NRF_SUCCESS)
	{
	   	printf("Error: %lu\n", retval);
	}
}

/**@brief Function for application main entry.
 */
int main(void)
{
	//uint32_t	err_code;
	bool	erase_bonds = 0;
	//
	// Initialize Shower Storage Index & Rollover Flag
	//
	m_qiqi.ShowerIndex = 0;
	m_qiqi.ShowerCycleRollOver = FALSE;
	//
	// System Initialize
	//
	#ifdef BOARD_CUSTOM
		init_uart();
	#endif

	timers_init();
    gpio_init();
    ble_stack_init();

    device_manager_init(erase_bonds);

    init_p_storage();

    read_DeviceName();

    gap_params_init();

    services_init();
    advertising_init();
    conn_params_init();
    app_bond_init(&m_app_bond_table);

    TempSense_adc_init();

    //timers_start();  // No need to do this because Timers get started individually as needed

    //
    // Start Advertising
    //
    sd_ble_gap_tx_power_set(4);

    advertising_start();

    //
    // Restart Advertising so that an App or Remote can connect after the first power up
    //
    //ble_advertising_restart_without_whitelist();
    //
    // Enter Ready State
    //
    Enter_Ready_State();
    //
    // Get Device UID
    //
    get_DeviceAddress(&ble_gap_addr);

#ifdef BOARD_CUSTOM
	printf("DeviceID:");

	for (int a = 0; a < 6; a++)
    	printf("%x", ble_gap_addr.addr[a]);

	printf("\n");

   	printf("Hello World of QIQI\n");
#endif

    Flash_LED(5, 250);
    //
    // Enter main loop
    //
    for (;;)
    {
        power_manage();
    }
}
