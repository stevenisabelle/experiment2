/* Copyright (c) 2016 QIQI Labs All Rights Reserved.
 *
 *
 */
#ifndef BLE_LBS_H__
#define BLE_LBS_H__

#include <stdint.h>
#include <stdbool.h>
#include "ble.h"
#include "ble_srv_common.h"
//
// QIQI Commands
//
#define CMD_SENDSTATUS				1
#define CMD_ENTERWARMUPSTATE		2
#define CMD_ENTERWAITSTATE			3
#define CMD_SENDTEMPTOAPP			4
#define CMD_SENDVOLTSTOAPP			5
#define CMD_ENTERREADYSTATE			6
#define CMD_ENTERONSTATE			7
#define CMD_SETSHOWERLIMIT			8
#define CMD_SOFTBUTTONPRESS			9
#define CMD_SETQIQIMODE				10
#define CMD_GETTIMEFROMAPP			11
#define CMD_SENDTIMETOAPP			12
#define CMD_SETSHOWERONTIME			13
#define CMD_SETDEVICENAME			14
#define CMD_SENDDEVICEUID			15
#define CMD_SYSTEMRESET				16
#define CMD_SWITCHADVMODE			17
#define	CMD_SET_PRECHECK_TEMP		18
#define CMD_SENDNUMSTOREDUNSHOWERS	19
#define CMD_SENDSHOWERCYCLEDATA		20
#define CMD_SENDSHOWERVOLTSANDTEMP	21
#define CMD_SHOWERCYCLERESET		22
#define CMD_SENDBATTERYVOLTAGETEST	24
#define CMD_SETSHOWERISHOTTEMP		25
//
// Alert Definitions - These are to notify the App of State Changes and anomalies occurring on the Device
//
#define ALERT_DEVICE_ENTERED_READY_STATE     0x01
#define ALERT_DEVICE_ENTERED_WARMUP_STATE    0x02
#define ALERT_DEVICE_ENTERED_WAIT_STATE      0x03
#define ALERT_TEMPERATURE_UPDATE             0x04
#define ALERT_DEVICE_VOLTAGE_UPDATE			 0x05
#define ALERT_BATTERY_LEVEL_LOW_UPDATE		 0x06
#define ALERT_DEVICE_ENTERED_ON_STATE        0x07
#define ALERT_SHOWER_DURATION_ON             0x08
#define ALERT_COLD_WATER_ABORT               0x09
#define ALERT_BATTERY_LEVEL_CRITICAL         0x0A
#define ALERT_PASSWORD_SET_UNSUCCESSFUL      0x0B
#define ALERT_SEND_DEVICE_TIME				 0x0C
#define ALERT_AUTO_WARMUP_TIME_REACHED		 0x0D

#define ALERT_SEND_DEVICE_ADDR				 0x0F
#define ALERT_SHOWER_SCALDING				 0x10
#define ALERT_SWITCH_ADV_MODE				 0x11

#define ALERT_SEND_NUM_STORED_SHOWERS		 0x13
#define ALERT_SENDSHOWERTIMESTAMP			 0x14
#define ALERT_SENDSHOWERCYCLETIMES			 0x15
#define ALERT_SHOWERCYCLEVOLTAGE			 0x16
#define ALERT_SHOWERCYCLETEMPERATURE		 0x17
#define ALERT_DEVICE_BATTERY_TEST_UPDATE	 0x18
#define ALERT_SHOWERISHOTTEMPSET			 0x19
#define ALERT_PRECHECKTEMPSET				 0x1A
//
//  QIQI UUID
//
#define QIQI_UUID_BASE 			{0x1F, 0x10, 0xC7, 0x6B, 0xEA, 0x3F, 0x40, 0x26, 0x84, 0xE5, 0x66, 0x58, 0x1C, 0xC4, 0xD1, 0x6E}
#define QIQI_UUID_SERVICE 		0xC41C
#define QIQI_UUID_SEND_CHAR		0xC41D
#define QIQI_UUID_GET_CHAR 		0xC41E

//#define LBS_UUID_BASE  {0x23, 0xD1, 0xBC, 0xEA, 0x5F, 0x78, 0x23, 0x15, 0xDE, 0xEF, 0x12, 0x12, 0x00, 0x00, 0x00, 0x00}
//#define LBS_UUID_SERVICE 		0x1523
//#define LBS_UUID_LED_CHAR 	0x1525
//#define LBS_UUID_BUTTON_CHAR 	0x1524

#define HIGH							1
#define LOW								0
#define TRUE							1
#define FALSE							0
#define SOFT							0
#define	HARD							1
#define STATE_TAG  						0x01

#define NUM_STORED_SHOWERS				4

// Forward declaration of the ble_qiqi_t type.
typedef struct ble_qiqi_s ble_qiqi_t;

typedef void (*ble_qiqi_app_command_handler_t) (ble_qiqi_t * p_qiqi, uint8_t new_state);

typedef struct
{
  ble_qiqi_app_command_handler_t app_command_handler;    /**< Event handler to be called when the QIQI characteristic is written. */
} ble_qiqi_init_t;

/**@brief QIQI Service structure. This contains various status information for the service. */
typedef struct ble_qiqi_s
{
    uint16_t                    service_handle;
    ble_gatts_char_handles_t    send_to_app_char_handles;
    ble_gatts_char_handles_t    get_from_app_char_handles;
    uint8_t                     uuid_type;
    uint8_t						SystemState;
    uint8_t						ShowerTemp;
    uint8_t						ShowerIsHotTemp;
    uint8_t						PrecheckTemp;
    uint8_t						ShowerDuration;
    uint8_t						ShowerIndex;			//NEW!!! EQ 2-11-17
	uint8_t						ShowerCycleRollOver; 	//NEW!!! EQ 2-12-17
    uint8_t						AppBuffer[30];
    uint16_t                    conn_handle;
    uint8_t						DeviceTime[30];
    uint8_t						BatteryVoltage[8];
    uint16_t					AdvertisingMode;
    uint8_t						DeviceAddr[BLE_GAP_ADDR_LEN];
    ble_qiqi_app_command_handler_t app_command_handler;
} ble_qiqi_t;

/** Shower Cycle Structure */

typedef struct ble_qiqi_shower_cycle_s
{
	  unsigned long long 		Warmup_Start;
	  char						Warmup_Start_ASCII[20];
	  char						CycleTimes_ASCII[20];
	  char						CycleVoltage[8];
	  uint8_t					WarmupASCII_Length;
	  uint8_t					CycleTimes_ASCII_Length;
	  uint16_t 					Warmup_Time;
	  uint16_t					Wait_Time;
	  uint16_t					On_Time;
	  uint8_t					BufferPointer;
	  uint8_t					BufferLength;
	  uint8_t					CycleTemp;
} ble_qiqi_shower_cycle_t;

/**@brief Function for initializing the QIQI Service.
 *
 * @param[out]  p_qiqi       QIQI Service structure. This structure will have to be supplied by
 *                           the application. It will be initialized by this function, and will later
 *                           be used to identify this particular service instance.
 * @param[in]   p_qiqi_init  Information needed to initialize the service.
 *
 * @return      NRF_SUCCESS on successful initialization of service, otherwise an error code.
 */
uint32_t ble_qiqi_init(ble_qiqi_t * p_qiqi, const ble_qiqi_init_t * p_qiqi_init);

/**@brief Function for handling the Application's BLE Stack events.
 *
 * @details Handles all events from the BLE stack of interest to the QIQI Service.
 *
 *
 * @param[in]   p_qiqi     QIQI Service structure.
 * @param[in]   p_ble_evt  Event received from the BLE stack.
 */
void ble_qiqi_on_ble_evt(ble_qiqi_t * p_qiqi, ble_evt_t * p_ble_evt);

void ble_update_ShowerCycles(char* newString, ble_qiqi_t * p_qiqi);

/**@brief Function for sending a button state notification.
 */
uint32_t ble_transmit_data_to_app(ble_qiqi_t * p_qiqi, uint16_t button_state);

//
// Shower Cycle Parameters
//
ble_qiqi_shower_cycle_t			m_Shower_Cycle[NUM_STORED_SHOWERS];

#endif // BLE_LBS_H__

/** @} */
