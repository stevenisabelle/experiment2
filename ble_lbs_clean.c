/*
 *
 * Copyright (c) 2016 QIQI Labs. All Rights Reserved.
 *
 */
#include <string.h>
#include "ble_lbs.h"
#include "nordic_common.h"
#include "ble_srv_common.h"
#include "app_util.h"

/**@brief Function for handling the Connect event.
 *
 * @param[in]   p_qiqi      Service structure.
 * @param[in]   p_ble_evt   Event received from the BLE stack.
 */
static void on_connect(ble_qiqi_t * p_qiqi, ble_evt_t * p_ble_evt)
{
    p_qiqi->conn_handle = p_ble_evt->evt.gap_evt.conn_handle;

}

/**@brief Function for handling the Disconnect event.
 *
 * @param[in]   p_qiqi      Service structure.
 * @param[in]   p_ble_evt   Event received from the BLE stack.
 */
static void on_disconnect(ble_qiqi_t * p_qiqi, ble_evt_t * p_ble_evt)
{
    UNUSED_PARAMETER(p_ble_evt);
    p_qiqi->conn_handle = BLE_CONN_HANDLE_INVALID;
    //advertising_start();
}

/**@brief Function for handling the Write event.
 *
 * @param[in]   p_qiqi      Service structure.
 * @param[in]   p_ble_evt   Event received from the BLE stack.
 */
static void on_write(ble_qiqi_t * p_qiqi, ble_evt_t * p_ble_evt)
{
	int i;

    ble_gatts_evt_write_t * p_evt_write = &p_ble_evt->evt.gatts_evt.params.write;
    
	if ((p_evt_write->handle == p_qiqi->send_to_app_char_handles.value_handle) && (p_qiqi->app_command_handler != NULL))
    {
    	//printf("Length: %d\n", p_evt_write->len);

    	for (i = 0; i < p_evt_write->len; i ++)
    	{
    		p_qiqi->AppBuffer[i] = p_evt_write->data[i];
    	}

        p_qiqi->app_command_handler(p_qiqi, p_evt_write->data[0]);
    }
}

void ble_qiqi_on_ble_evt(ble_qiqi_t * p_qiqi, ble_evt_t * p_ble_evt)
{
    switch (p_ble_evt->header.evt_id)
    {
        case BLE_GAP_EVT_CONNECTED:
            on_connect(p_qiqi, p_ble_evt);
            break;
            
        case BLE_GAP_EVT_DISCONNECTED:
            on_disconnect(p_qiqi, p_ble_evt);
            break;
            
        case BLE_GATTS_EVT_WRITE:
            on_write(p_qiqi, p_ble_evt);
            break;
            
        default:
            // No implementation needed.
            break;
    }
}

/**@brief Function for adding send_to_app characteristic.
 *
 */
static uint32_t send_to_app_char_add(ble_qiqi_t * p_qiqi, const ble_qiqi_init_t * p_qiqi_init)
{
    ble_gatts_char_md_t char_md;
    ble_gatts_attr_t    attr_char_value;
    ble_uuid_t          ble_uuid;
    ble_gatts_attr_md_t attr_md;

    memset(&char_md, 0, sizeof(char_md));
    
    char_md.char_props.read   = 1;
    char_md.char_props.write  = 1;
    char_md.p_char_user_desc  = NULL;
    char_md.p_char_pf         = NULL;
    char_md.p_user_desc_md    = NULL;
    char_md.p_cccd_md         = NULL;
    char_md.p_sccd_md         = NULL;
    
    ble_uuid.type = p_qiqi->uuid_type;
    ble_uuid.uuid = QIQI_UUID_SEND_CHAR;
    
    memset(&attr_md, 0, sizeof(attr_md));

    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&attr_md.read_perm);
    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&attr_md.write_perm);
    attr_md.vloc       = BLE_GATTS_VLOC_STACK;
    attr_md.rd_auth    = 0;
    attr_md.wr_auth    = 0;
    attr_md.vlen       = 1;
    
    memset(&attr_char_value, 0, sizeof(attr_char_value));

    attr_char_value.p_uuid       = &ble_uuid;
    attr_char_value.p_attr_md    = &attr_md;
    attr_char_value.init_len     = 20;
    attr_char_value.init_offs    = 0;
    attr_char_value.max_len      = 40;
    attr_char_value.p_value      = NULL;
    
    return sd_ble_gatts_characteristic_add(p_qiqi->service_handle, &char_md,
                                               &attr_char_value,
                                               &p_qiqi->send_to_app_char_handles);
}

/**@brief Function for adding the get_from_app characteristic.
 *
 */
static uint32_t get_from_app_char_add(ble_qiqi_t * p_qiqi, const ble_qiqi_init_t * p_qiqi_init)
{
    ble_gatts_char_md_t char_md;
    ble_gatts_attr_md_t cccd_md;
    ble_gatts_attr_t    attr_char_value;
    ble_uuid_t          ble_uuid;
    ble_gatts_attr_md_t attr_md;

    memset(&cccd_md, 0, sizeof(cccd_md));

    BLE_GAP_CONN_SEC_MODE_SET_ENC_NO_MITM(&cccd_md.write_perm); // This works with iPhone 5/5/2016
    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&cccd_md.read_perm);

    cccd_md.vloc = BLE_GATTS_VLOC_STACK;
    
    memset(&char_md, 0, sizeof(char_md));
    
    char_md.char_props.read   = 1;
    char_md.char_props.notify = 1;
    char_md.p_char_user_desc  = NULL;
    char_md.p_char_pf         = NULL;
    char_md.p_user_desc_md    = NULL;
    char_md.p_cccd_md         = &cccd_md;
    char_md.p_sccd_md         = NULL;
    
    ble_uuid.type = p_qiqi->uuid_type;
    ble_uuid.uuid = QIQI_UUID_GET_CHAR;
    
    memset(&attr_md, 0, sizeof(attr_md));

    BLE_GAP_CONN_SEC_MODE_SET_ENC_WITH_MITM(&attr_md.write_perm);  // This works with the iPhone 5/5/2016
    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&attr_md.read_perm);

    attr_md.vloc       = BLE_GATTS_VLOC_STACK;
    attr_md.rd_auth    = 0;
    attr_md.wr_auth    = 0;
    attr_md.vlen       = 1;
    
    memset(&attr_char_value, 0, sizeof(attr_char_value));

    attr_char_value.p_uuid       = &ble_uuid;
    attr_char_value.p_attr_md    = &attr_md;
    attr_char_value.init_len     = 20;//sizeof(uint8_t);
    attr_char_value.init_offs    = 0;
    attr_char_value.max_len      = 40;
    attr_char_value.p_value      = NULL;
    
    return sd_ble_gatts_characteristic_add(p_qiqi->service_handle, &char_md,
                                               &attr_char_value,
                                               &p_qiqi->get_from_app_char_handles);
}


uint32_t ble_qiqi_init(ble_qiqi_t * p_qiqi, const ble_qiqi_init_t * p_qiqi_init)
{
    uint32_t   err_code;
    ble_uuid_t ble_uuid;
    //
    // Initialize service structure
    //
    p_qiqi->conn_handle       = BLE_CONN_HANDLE_INVALID;
    p_qiqi->app_command_handler = p_qiqi_init->app_command_handler;
    //
    // Add UUID
    //
    ble_uuid128_t base_uuid = {QIQI_UUID_BASE};

    err_code = sd_ble_uuid_vs_add(&base_uuid, &p_qiqi->uuid_type);
    if (err_code != NRF_SUCCESS)
    {
        return err_code;
    }
    //
    // Add Service
    //
    ble_uuid.type = p_qiqi->uuid_type;
    ble_uuid.uuid = QIQI_UUID_SERVICE;

    err_code = sd_ble_gatts_service_add(BLE_GATTS_SRVC_TYPE_PRIMARY, &ble_uuid, &p_qiqi->service_handle);
    if (err_code != NRF_SUCCESS)
    {
        return err_code;
    }
    
    err_code = get_from_app_char_add(p_qiqi, p_qiqi_init);
    if (err_code != NRF_SUCCESS)
    {
        return err_code;
    }
    
    err_code = send_to_app_char_add(p_qiqi, p_qiqi_init);
    if (err_code != NRF_SUCCESS)
    {
        return err_code;
    }
    
    return NRF_SUCCESS;
}

uint32_t ble_transmit_data_to_app(ble_qiqi_t * p_qiqi, uint16_t Alert)
{
    ble_gatts_hvx_params_t params;
    uint8_t transmitBuffer[30];
	uint16_t len;

	//printf("Sending Command to App: %d\n", Alert);

	switch(Alert)
    {
      case STATE_TAG:

    	  len  = sizeof(uint8_t) * 2;

    	  transmitBuffer[0] = (uint8_t) STATE_TAG;
    	  transmitBuffer[1] = p_qiqi->SystemState;
    
    	break;

      case ALERT_TEMPERATURE_UPDATE: //Temperature Update

    	  len = sizeof(uint8_t) * 2;

 	      transmitBuffer[0] = (uint8_t) ALERT_TEMPERATURE_UPDATE;
    	  transmitBuffer[1] = p_qiqi->ShowerTemp;

    	  break;

      case ALERT_SHOWER_DURATION_ON: // Shower Duration Setting Update

    	  len = sizeof(uint8_t) * 2;

	      transmitBuffer[0] = (uint8_t) ALERT_SHOWER_DURATION_ON;
    	  transmitBuffer[1] = p_qiqi->ShowerDuration;

    	  break;

      case ALERT_SEND_DEVICE_TIME:

    	  len = strlen((const char*)p_qiqi->DeviceTime) + 2;

	      transmitBuffer[0] = (uint8_t) ALERT_SEND_DEVICE_TIME;
	      transmitBuffer[1] = (uint8_t) strlen((const char*)p_qiqi->DeviceTime);

	      memcpy(&transmitBuffer[2], p_qiqi->DeviceTime, transmitBuffer[1]);

    	  break;

      case ALERT_SEND_DEVICE_ADDR:

     	  len = BLE_GAP_ADDR_LEN + 2;

 	      transmitBuffer[0] = (uint8_t) ALERT_SEND_DEVICE_ADDR;
 	      transmitBuffer[1] = (uint8_t) BLE_GAP_ADDR_LEN;

 	      memcpy(&transmitBuffer[2], p_qiqi->DeviceAddr, transmitBuffer[1]);

     	  break;

      case ALERT_AUTO_WARMUP_TIME_REACHED:

      	  len  = sizeof(uint8_t);

      	  transmitBuffer[0] = (uint8_t) ALERT_AUTO_WARMUP_TIME_REACHED;

      	  break;

      case ALERT_DEVICE_VOLTAGE_UPDATE:

    	  len = strlen((const char*)p_qiqi->BatteryVoltage) + 2;

    	  transmitBuffer[0] = (uint8_t) ALERT_DEVICE_VOLTAGE_UPDATE;
    	  transmitBuffer[1] = (uint8_t) strlen((const char*)p_qiqi->BatteryVoltage);

    	  memcpy(&transmitBuffer[2], p_qiqi->BatteryVoltage, transmitBuffer[1]);

    	  break;

      case ALERT_BATTERY_LEVEL_LOW_UPDATE:

          len = strlen((const char*)p_qiqi->BatteryVoltage) + 2;

          transmitBuffer[0] = (uint8_t) ALERT_BATTERY_LEVEL_LOW_UPDATE;
          transmitBuffer[1] = (uint8_t) strlen((const char*)p_qiqi->BatteryVoltage);

          memcpy(&transmitBuffer[2], p_qiqi->BatteryVoltage, transmitBuffer[1]);

          break;

      case ALERT_BATTERY_LEVEL_CRITICAL:

          len = strlen((const char*)p_qiqi->BatteryVoltage) + 2;

          transmitBuffer[0] = (uint8_t) ALERT_BATTERY_LEVEL_CRITICAL;
          transmitBuffer[1] = (uint8_t) strlen((const char*)p_qiqi->BatteryVoltage);

          memcpy(&transmitBuffer[2], p_qiqi->BatteryVoltage, transmitBuffer[1]);

          break;

      case ALERT_SHOWER_SCALDING:

       	  len  = sizeof(uint8_t);

       	  transmitBuffer[0] = (uint8_t) ALERT_SHOWER_SCALDING;

       	  break;

      case ALERT_SWITCH_ADV_MODE:

    	  len = sizeof(uint8_t) * 2;

    	  transmitBuffer[0] = (uint8_t) ALERT_SWITCH_ADV_MODE;
          transmitBuffer[1] = (uint8_t) p_qiqi->AdvertisingMode;

    	  break;

      default:

    	  break;
    }

	memset(&params, 0, sizeof(params));
	params.type = BLE_GATT_HVX_NOTIFICATION;
	params.handle = p_qiqi->get_from_app_char_handles.value_handle;

    params.p_data = transmitBuffer;

	params.p_len =  &len;

	return sd_ble_gatts_hvx(p_qiqi->conn_handle, &params);
}
